
const express = require('express')
var mongoose = require('mongoose')
var helmet = require('helmet')
var https = require('https')
var bodyParser = require('body-parser')
var auth = require('./Filters/auth.js').auth
var settings = require('./global/settings.js')
var time = require('./controllers/time.js')
var data = require('./controllers/data.js')
var display = require('./controllers/display.js')
var configuration = require('./controllers/configuration.js')
var managment = require('./controllers/managment.js')
var users = require('./controllers/users.js')
var fs = require('fs');

var app = express()
mongoose.connect(settings.MongoDBConnection,{useMongoClient: true});


var options = {
   key  : fs.readFileSync('server.key'),
   cert : fs.readFileSync('server.crt')
};

app.use(helmet())
app.set('port', process.env.PORT || settings.HttpsWebPort);
app.set('host', process.env.HOST || settings.ListenIp);

app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(bodyParser.json());

app.get('/',function (req, res) {res.send('Welcome to Envidas Embedded')})

/*Time section*/
app.get('/time',auth,time.getTime)
app.post('/time',auth,time.postTime)	

/*Data section*/
app.get('/LatestData',auth,data.getLatestData)
app.get('/LatestData/:id',auth,data.getLatestDataDevice)
app.get('/Data',auth,data.getData)
app.get('/Data/:id',auth,data.getDataDevice)
app.get('/DataFilesSize',auth,data.getFilesSize)
app.get('/DataFilesSize/:id',auth,data.getFilesSizeDevice)
app.get('/Data/:id/Channel/:channel',auth,data.getChannelData)

/*Display*/
app.get('/DisplayDevices',auth,display.getDevicesDisplay)
app.get('/DisplayDevices/:id',auth,display.getDeviceDisplay)
app.get('/DisplayDevices/:id/Channel/:channel',auth,display.getChannelDisplay)


/*Configuration*/
app.get('/Configuration',auth,configuration.getConfiguration)
app.post('/Configuration',auth,configuration.postConfiguration)

/*Managment request*/
app.get('/Error/:day',auth,managment.getErrorLogFile)
app.post('/Backup',auth,managment.postBackUpFiles)

/*This one is only for insert users to mongo*/
app.post('/User',users.post)


https.createServer(options, app).listen(app.get('port'), function () {
   console.log('Example app listening on port '+ app.get('port'));
});


console.log("Server running at https://"+settings.ListenIp +":"+ settings.HttpsWebPort);
