var basicAuth = require('basic-auth');

var url = require('../global/settings.js').MongoDBConnection

var UsersFromMongo = require('../models/user.js');



exports.auth = function (req, res, next) {
  var user = basicAuth(req);
  
  if (!user || !user.name || !user.pass) {
    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    res.sendStatus(401);
  }

  UsersFromMongo.findOne({ $and: [{user:user.name},{pass:user.pass}]}, function(error, dbUser) {
    if(dbUser==null)
    {
       res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
       res.sendStatus(401);
    }
    req.params.isAdmin = dbUser.isAdmin;
    next();
  })
}
