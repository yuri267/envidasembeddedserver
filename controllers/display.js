var model = require('../models/display');

exports.getDevicesDisplay = function(req,res){
	model.DevicesDisplay(req,res);
}

exports.getDeviceDisplay = function(req,res){
	model.DeviceDisplay(req,res);
}

exports.getChannelDisplay = function(req,res){
	model.ChannelDisplay(req,res);
}