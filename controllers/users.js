var User = require('../models/user.js');

exports.post = function(req, res) {
    var u = new User({user: req.body.user, pass: req.body.pass, isAdmin : req.body.isAdmin});
    u.save();
    res.send(JSON.stringify({message : u._id}));
}