var model = require('../models/data');

exports.getLatestData = function(req,res){
	model.getLatest(req,res);
	}

exports.getLatestDataDevice = function(req,res){
	model.getDeviceLatest(req,res);
	}

exports.getData = function(req,res){
	model.getData(req,res)
	}

exports.getDataDevice = function(req,res){
	model.getDeviceData(req,res)
	}

exports.getFilesSize = function(req,res){
	model.getDataFileSize(req,res);
	}

exports.getFilesSizeDevice = function(req,res){
	model.getDeviceDataFileSize(req,res);
	}

exports.getChannelData = function(req,res){
	model.getChannelData(req,res);
	}