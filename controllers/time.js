var commands = require('../models/systemCommands');

exports.getTime = function(req,res){
	var datetime =  new Date();
	console.log('maxim ' +datetime.toString());
	res.setHeader('Content-Type','application/json');
	res.send(JSON.stringify({
	date : datetime.toString()})); 
}

exports.postTime = function(req,res){
	if(req.params.isAdmin)
		commands.setTimeCommand(req,res);	
	else
	{
		res.status(401);
		res.send(JSON.stringify({
		message : 'Not sufficient credentials'})); 
	}	
}

