var model = require('../models/configuration');

exports.getConfiguration = function(req,res){
	model.readConfiguration(req,res);
}

exports.postConfiguration = function(req,res){
	if(req.params.isAdmin)
		model.saveConfiguration(req,res);
	else
	{
		res.status(401);
		res.send(JSON.stringify({
		message : 'Not sufficient credentials'})); 
	}

}