#include "rapidjson/reader.h"
#include "rapidjson/document.h"
#include <iostream>

using namespace rapidjson;



int main() {
    using namespace std;


    FILE *fileptr;

    long filelen;
	fileptr = fopen("configuration.json", "rb");  // Open the file in binary mode
	fseek(fileptr, 0, SEEK_END);          // Jump to the end of the file
	filelen = ftell(fileptr);             // Get the current byte offset in the file
	rewind(fileptr);                      // Jump back to the beginning of the file

	auto str = (char *)malloc((filelen+1)*sizeof(char)); // Enough memory for file + \0
	fread(str, filelen, 1, fileptr); // Read in the entire file
	fclose(fileptr); // Close the file
	


    Document document;
    document.Parse<0>(str).HasParseError();
    document.Parse(str);
if(document.IsObject())        // add this line
{
    Value& stationName = document["name"];
    cout<<stationName.GetString()<<endl;

    stationName = document["id"];
    cout<<stationName.GetDouble()<<endl;

    stationName = document["latitude"];
    cout<<stationName.GetString()<<endl;
    
    stationName = document["longitude"];
    cout<<stationName.GetString()<<endl;

    Value& devices = document["devices"];
    
    
    for (int i = 0; i < devices.Size(); ++i)
    {   
       cout<<"-------------------------------"<<endl;
       auto& second_element = devices[i]; 
       string elem = second_element["id"].GetString(); 
       cout<<"Device id : "<<elem<<endl;
       elem = second_element["additionalData"].GetString(); 
       cout<<"additionalData :"<<elem<<endl;
       elem = second_element["Address"].GetString(); 
       cout<<"Address :"<<elem<<endl;
       auto& telem = second_element["sampleRate"]; 
       cout<<"sampleRate :"<<telem.GetDouble()<<endl;
       telem = second_element["AvgTime"]; 
       cout<<"AvgTime :"<<telem.GetDouble()<<endl;
       telem = second_element["port"]; 
       cout<<"port :"<<telem.GetDouble()<<endl;
       telem = second_element["type"]; 
       cout<<"type :"<<telem.GetDouble()<<endl;

       auto& channelsElem = second_element["channels"];
       for (int j = 0; j < channelsElem.Size(); ++j)
       {
            auto& element = channelsElem[j]; 
            cout<<"===========channel=============="<<endl;
            auto& celem = element["id"]; 
            cout<<"id :"<<celem.GetDouble()<<endl;
            celem = element["linearA"]; 
            cout<<"linearA :"<<celem.GetDouble()<<endl;
            celem = element["linearB"]; 
            cout<<"linearB :"<<celem.GetDouble()<<endl;
            celem = element["minAvg"]; 
            cout<<"minAvg :"<<celem.GetDouble()<<endl;
       }

   }

}
return 0;
}




RAPIDJSON_DIAG_POP
