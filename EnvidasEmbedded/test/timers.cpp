#include <iostream>
#include <thread>
#include <chrono>
#include <future>
#include <iostream>

using namespace std;

struct thread_data
{
   int  id;
   string name;
};

struct Foo
{
  Foo() : data(0) {}
  void sum(int i) { data +=i;}
  int data;
};

class Timer
{
    thread th;
    bool running;
    
public:
    typedef std::chrono::milliseconds Interval;
    typedef std::function<void(void)> Timeout;

    Timer(){	running= false;}
    void start(const Interval &interval,
               const Timeout &timeout)
    {
        running = true;

        th = thread([=]()
        {
            while (running == true) {
                this_thread::sleep_for(interval);
                timeout();
            }
        });

// [*]
        th.join();
    }

    void stop()
    {
        running = false;
    }
};

void print(string name)
{
  Foo foo;
  auto f = std::async(&Foo::sum, &foo, 42);
  f.get();
  std::cout << foo.data << " "<<name <<"\n";
}

int main()
{
    thread_data data;
data.id = 1;
data.name = "da";

    Timer tHello;
    tHello.start(chrono::seconds(5), []{
       print("yuri");
    });
	
	std::cout <<"asd"<<std::endl;
    this_thread::sleep_for(chrono::seconds(2));
    tHello.stop();
    return 1;
}
