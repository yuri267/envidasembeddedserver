
#include "runnable/timersHandler.hpp"
#include "runnable/CreateStation.hpp"
#include "runnable/SetProtocol.hpp"

using namespace std;

bool adr1500::IsAdrFirstTime=true;

int main () {

   station sta = stationRetrail();
   vector<device> devices = sta.getDevices();

   int numOfDevices = devices.size();
   int numberOfThreads = numOfDevices * 2;

   thread t[numberOfThreads];

   int i=0;

   errorLog er;
   er.saveStringToBufferw("Staring Poller");

   for (device dev : devices) 
   {
      thread_data td;
      td.thread_id = i;
      td.protocol = ProtocolFactory::make_protocol(dev);
      td.dev = dev;

      thread_data tdAvg;
      tdAvg.thread_id = i;
      tdAvg.dev = dev;

      t[i++] = thread(StartPollDataTimer, td);
      t[i++] = thread(StartAverageTimer, tdAvg);     
   }

   for (int i = 0; i < numberOfThreads; ++i) {t[i].join();}

   return 1;
}

