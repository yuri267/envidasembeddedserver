#ifndef Channel_Hpp
#define Channel_Hpp

#include "errorLog.hpp"
#include "../runnable/SetAverage.hpp"

class Channel
{
public:
	IAverage * Average;
	Channel(){}

	Channel(int id, float linearA,float linearB,float minAvg,
	int averageType,int ThreasholdPercentage,int additionchannels,string units,
	string ChannelName,string additionData,string formula,bool AddSummary){
		
		this->linearA  = linearA;
		this->linearB  = linearB;
		this->minAvg  = minAvg;
		this->averageType  = averageType;
		this->ThreasholdPercentage  = ThreasholdPercentage;
		this->id  = id;
		this->additionchannels  = additionchannels;
		this->units  = units;
		this->ChannelName  = ChannelName;
		this->additionData  = additionData;
		this->formula  = formula;
		this->AddSummary = AddSummary;
		this->Average=make_average(averageType,AddSummary);
	}
	~Channel(){}

	int get_id(){
		return id;
	}

	int get_AverageType() {
		return averageType;
	}
	int get_Threshold_Percentage() {
		return ThreasholdPercentage;
	}
	float get_LinearA() {
		return linearA;
	}
	float get_LinearB() {
		return linearB;
	}
	int get_additionchannels() {
		string str = get_additionData();
		int index = 0, indexEnd;

		int ret;
		indexEnd = str.find("?", index);
		ret = atoi(str.substr(index, indexEnd - index).c_str());
		return ret;
	}

	string get_additionData() {
		return additionData;
	}
	float get_minAvg() {
		return minAvg;
	}

	bool get_summary(){return AddSummary;}

	void get_sigmaTeta(int* arr) {
		int index = 0, indexEnd;
		string str = get_additionData();
		indexEnd = str.find(",", index);
		arr[0] = atoi(str.substr(index, indexEnd - index).c_str());
		index = indexEnd + 1;
		indexEnd = str.find("?", index);
		arr[1] = atoi(str.substr(index, indexEnd - index).c_str());
	}

	void get_StandatdCondChanels(int* arr) {
		int index = 0, indexEnd;
		string str = formula;
		indexEnd = str.find(",", index);
		arr[0] = atoi(str.substr(index, indexEnd - index).c_str());
		index = indexEnd + 1;
		if (arr[0] == 0) {
			int var;
			for (var = 1; var < 5; ++var) {
				arr[var] = 0;
			}

		} else {
			indexEnd = str.find(",", index);
			arr[1] = atoi(str.substr(index, indexEnd - index).c_str());
			index = indexEnd + 1;
			indexEnd = str.find(",", index);
			arr[2] = atoi(str.substr(index, indexEnd - index).c_str());
			index = indexEnd + 1;
			indexEnd = str.find(",", index);
			arr[3] = atoi(str.substr(index, indexEnd - index).c_str());
			index = indexEnd + 1;
			indexEnd = str.find(",", index);
			arr[4] = atoi(str.substr(index, indexEnd - index).c_str());
			index = indexEnd + 1;
			indexEnd = str.find("?", index);
			arr[5] = atoi(str.substr(index, indexEnd - index).c_str());
		}
	}
private:
	float linearA;
	float linearB;
	float minAvg;
	int averageType;
	int ThreasholdPercentage;
	int id;
	int additionchannels;
	string units;
	string ChannelName;
	string additionData;
	string formula;
	bool AddSummary;
	
};

#endif /* Channel_Hpp */