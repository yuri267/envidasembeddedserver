#ifndef station_H_
#define station_H_

#include <vector>
#include "device.hpp"

using namespace std;
class station {

public:
	station(){}

	station(string name,int id,string lat,string longi,vector<device> devs){
		this->name=name;
		this->id=id;
		this->latitude=lat;
		this->longitude=longi;
		this->devices = devs;
	}

	string getname() {
		return this->name;
	}

	int numberOfDevices()
	{
		return devices.size();
	}

	device getDevice(int i)
	{
		return devices.at(i);
	}

	vector<device> getDevices()
	{
		return devices;
	}
private:
	string name;
	int id;
	string latitude,longitude;
	vector<device> devices;

};
#endif /* station_H_ */