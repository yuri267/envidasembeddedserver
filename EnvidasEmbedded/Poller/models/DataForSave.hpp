#ifndef DataForSave_HPP
#define DataForSave_HPP

#include "Average/Summary.hpp"

class DataForSave 
{
public:

	vector<float> values;
	vector<int> status;
	vector<Summary> summary;
	int len;

	DataForSave(){}
	
	~DataForSave(){}
	
	DataForSave(int size){
		len=size;
		values.resize(size);
		status.resize(size);
		summary.resize(size);
	}
};



#endif /* DataForSave_HPP */