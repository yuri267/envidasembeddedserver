#ifndef ERRORLOG_HPP_
#define ERRORLOG_HPP_

#include "DataStatus.hpp"
#include "Enums.hpp"
#include "global.hpp"
#include <mutex>
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

using namespace rapidjson;
using namespace std;

mutex mtx;

class errorLog
{
public:

	errorLog(){}

	~errorLog(){}

	static void saveStringToBufferw(string text) {

		if(text.length()<2)
			return;

		string buf = text + "!/\n";
		writeBuftofile(buf);
	}

private:

	static void writeBuftofile(string buf) {
		 mtx.lock();
		 writeToErrorLog(buf);
		 mtx.unlock();
	}

	static void writeToErrorLog(string buf) {
		 time_t t = time(0);
		 struct tm * now = localtime(&t);
		 
		StringBuffer s;
		Writer<StringBuffer> writer(s);
		writer.StartObject(); 
		writer.Key("Date");

		 stringstream oss;
		 oss << now->tm_mday << "/" << (now->tm_mon + 1) << "/"
		 		<< (now->tm_year + 1900) << " " << now->tm_hour << ":";
		 if (now->tm_min < 10) {
		 	oss << "0";
		 }
		 oss << now->tm_min;

		writer.String(oss.str().c_str());
		
		writer.Key("Message");
		writer.String(buf.c_str());
		writer.EndObject();

		

		 stringstream logoSs;
		 logoSs<<s.GetString();
		 logoSs<<"\r\n";

		 stringstream css;
		 css <<workingFolder<<"/" << errorLogFile << now->tm_mday << ".txt";
		 string dayPath = css.str();

		 FILE * pFile;
		 pFile = fopen(dayPath.c_str(), "a");
		 fprintf(pFile, "%s", logoSs.str().c_str());
		 fclose(pFile);
	}

};
#endif /* ERRORLOG_HPP_ */