#ifndef DataContainer_HPP
#define DataContainer_HPP

#include "../models/device.hpp"
#include "../models/station.hpp"
#include "DataFile.hpp"
#include "dataSummarizer.hpp"

using namespace std;

class DataContainer
{
public:

	DataContainer(device devP){
		this->dev=devP;
	}

	~DataContainer(){}

	void AddObjectToStack(PollData pd){
		DataFile datafile;

		PollData Corrected = calcAXBCorrection(pd);

		datafile.writeInstanceToTxtFile(Corrected,this->dev.getId());
		dataStack.push(Corrected);

		Corrected.printPollData();
	}

	int StackSize(){
		return dataStack.size();
	}

	void AnalyzeAndSave(){

		DataForSave pdForSave = calcAvg();
		DataFile datafile;

		datafile.writeTxtWithSummary(pdForSave,this->dev.getId(),this->dev.getAvgTime());
		dataStack=stack<PollData>();
	}

	private : 
	device dev;
	stack<PollData> dataStack;

	DataForSave calcAvg(){
		dataSummarizer dS(this->dev);
		return dS.SummarizeData(dataStack);
	}

	PollData calcAXBCorrection(PollData pd){
		
		for(Channel channel : this->dev.getChannels())
		{
			int channelId = channel.get_id();
			if(pd.status.at(channelId)==1 && pd.values.at(channelId)!=-9999)
			{
				pd.values.at(channelId) = pd.values.at(channelId) * channel.get_LinearA() +
				channel.get_LinearB();
			}
			
		}

		return pd;
	}
};

#endif /* DataContainer_HPP */