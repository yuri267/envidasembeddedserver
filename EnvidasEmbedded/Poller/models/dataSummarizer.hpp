#ifndef DATASUMMARIZER_HPP_
#define DATASUMMARIZER_HPP_

#include "Average/AverageParams.hpp"


class dataSummarizer
{
public:
	dataSummarizer(device dev){
		this->dev = dev;
		this->Channels = dev.getChannels();
		this->averageParams.resize(Channels.size());
	}

	DataForSave SummarizeData(stack<PollData> stackData){
		DataForSave pd(Channels.size());

		if(stackData.size()==0)
		{
			return pd;
		}

		this->actualCycle=stackData.size();
		this->configCycle=dev.getAvgTime()*60/dev.getsampleRate();

		while (!stackData.empty()) {
			PollData pdTemp = stackData.top();
			stackData.pop();
			summarize(pdTemp);
		}

		
		for(Channel Channel : Channels)
		{
			int channelId = Channel.get_id();
			AverageParams averageParams = FinalizeAverage(Channel.Average);
			pd.values.at(channelId) = averageParams.value;
			pd.status.at(channelId) = ValidateTheStatus(averageParams,channelId);
			pd.summary.at(channelId) = averageParams.summary;
			resetsAverage(Channel.Average);
		}

		return pd;
	}
	
	~dataSummarizer(){}

private:

	device dev;
	int actualCycle, configCycle;
	vector<Channel> Channels;
	vector<AverageParams> averageParams;
	
	void summarize(PollData pd){
		
		for(Channel Channel : Channels)
		{
			int channelId = Channel.get_id();
			float* valuesArr;
			int* statusArr;
			getAdditionalData(Channel,valuesArr,statusArr,pd);

			Summarize(Channel.Average,pd.values.at(channelId),pd.status.at(channelId),valuesArr,statusArr);
		}
	}

	int ValidateTheStatus(AverageParams averageParams, int channelId){
		int retStatus = averageParams.status;
		auto percent = Channels.at(channelId).get_Threshold_Percentage();
		if((((double)actualCycle/(double)configCycle)*100)<percent)
			retStatus = DataStatus::LessThenSample;
		else if ((averageParams.validSampleCount + averageParams.validSampleCountMin)*100/(configCycle) < percent)
		{
			if((averageParams.validSampleCount + averageParams.validSampleCountMin)>0)
				retStatus = DataStatus::LessThenSample;
			else
				retStatus = DataStatus::InValid;
		}
		return retStatus;
	}

	void getAdditionalData(Channel channel,float* valuesArr,int* statusArr,PollData pd){
		
		 switch(channel.get_AverageType())
		 {
			case yamartinoStandartDeviation :
			{
				valuesArr[1];
				statusArr[1];
				int wsIndex = channel.get_additionchannels();
				valuesArr[0] = pd.values.at(wsIndex);
				statusArr[0] = pd.status.at(wsIndex);
			}
			break;
			case windspeed :
			{
				valuesArr[2];
				statusArr[1];
				int wsIndex = channel.get_additionchannels();
				valuesArr[0] = pd.values.at(wsIndex);
				valuesArr[1] = channel.get_minAvg();
				statusArr[0] = pd.status.at(wsIndex);

			}
			case winddirection :
			{
				valuesArr[2];
				statusArr[1];
				int wdIndex = channel.get_additionchannels();
				valuesArr[0] = pd.values.at(wdIndex);
				valuesArr[1] = channel.get_minAvg();
			}
			case unit_Vector_Direction :
			{
				valuesArr[2];
				statusArr[1];
				valuesArr[0] = 1;
				valuesArr[1] = channel.get_minAvg();
			}
			break;
		 	case sigma_Teta:
		 	{
				int arr[2];
				channel.get_sigmaTeta(arr);
				int wsIndex = arr[0];
				int wdIndex = arr[1];
				valuesArr[3];
				statusArr[2];
				valuesArr[0] = pd.values.at(wsIndex);
				valuesArr[1] = pd.values.at(wdIndex);
				valuesArr[2] = channel.get_minAvg();
				statusArr[0] = pd.status.at(wsIndex);
				statusArr[1] = pd.status.at(wdIndex);
		 	}
		 	break;


		 	default : break;
		 }
	}

};

#endif /* DATASUMMARIZER_HPP_ */