#ifndef PollData_HPP
#define PollData_HPP


#include "errorLog.hpp"

class PollData {

public:

	vector<float> values;
	vector<int> status;
	int len;

	PollData(){}

	PollData(int channelsNumber){
		ReSize(channelsNumber);
	}

	void ReSize(int size){
		len=size;
		values.resize(size);
		status.resize(size);
		for (int i = 0; i < size; ++i) {
			values[i]=-9999;
			status[i]=DataStatus::NoData;
		}
	}

	void printPollData(){
		std::cout<<std::endl;
		for (int i = 0; i < len; ++i) {
			std::cout<<values[i]<<" - "<<status[i]<<"\n";
		}
		time_t now = time(0);
		tm *ltm = localtime(&now);

		std::cout<<ltm->tm_year+1900<<"/"<<ltm->tm_mon+1<<"/"<<ltm->tm_mday<<" "<<ltm->tm_hour<<":"<<ltm->tm_min<<":"<<ltm->tm_sec<<"\n";
	}

	void commFailStatusToall(){
		for (int i = 0; i < len; ++i) {
			this->values.at(i)=-9999;
			this->status.at(i)=DataStatus::NoData;
		}
	}

	void SetStatusToall(int status){
		for (int i = 0; i < len; ++i)
		{
			this->status.at(i)=status;
		}
	}

	void SetValueToAll(float value){
		for (int i = 0; i < len; ++i)
		{
			this->values.at(i)=value;
		}
	}

	void initZeroValue(int size){
		len=size;
		values.resize(size);
		status.resize(size);
		for (int i = 0; i < size; ++i) {
			values[i]=0;
			status[i]=DataStatus::OK;
		}
	}


};

#endif /* PollData_HPP */