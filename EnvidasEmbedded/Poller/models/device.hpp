
#ifndef DEVICE_HPP_
#define DEVICE_HPP_

#include "Channel.hpp"



class device {

public:

	device(){}

	device(int id, int port, int address, int baudRate, int type, int sampleRate, int avgTime, string additionData,vector<Channel> channels){
		this->id=id;
		this->port = port;
		this->Address=address;
		this->baudRate=baudRate;
		this->type=type;
		this->sampleRate=sampleRate;
		this->avgTime=avgTime;
		this->additionData=additionData;
		this->channels=channels;
	}

	int getId() {
		return this->id;
	}
	int getPort() {
		return this->port;
	}
	int getAddress() {
		return this->Address;
	}
	int getBaundRate() {
		return this->baudRate;
	}
	int getType() {
		return this->type;
	}
	int getsampleRate() {
		return this->sampleRate;
	}
	int getAvgTime() {
		return this->avgTime;
	}
	string getAdditionData() {
		return this->additionData;
	}

	vector<Channel> getChannels(){
		return channels;
	}

private:
	int id;
	int port, Address, type, baudRate;
	int sampleRate,avgTime;
	string additionData;
	vector<Channel> channels;
};

#endif /* DEVICE_HPP_ */