#ifndef DATAFILE_HPP_
#define DATAFILE_HPP_

#include "DataForSave.hpp"
#include "PollData.hpp"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

using namespace rapidjson;

class DataFile
{
public:
	DataFile(){}

	~DataFile(){}

	static void writeInstanceToTxtFile(PollData polld,int devId){
		time_t now = time(0);
		long long timeInMinutes=long(now);

		StringBuffer s;
		Writer<StringBuffer> writer(s);
		writer.StartObject(); 
		writer.Key("time");
		writer.Double(timeInMinutes); 
		writer.Key("channels");
		writer.StartArray();
		for (unsigned i = 0; i < polld.len; i++)
		{
			writer.StartObject();
			writer.Key("id");
			writer.Uint(i);
			writer.Key("value");

			ostringstream ss;
			ss << polld.values[i];
			string s(ss.str());

			writer.String(s.c_str());
			writer.Key("status");
			writer.Uint(polld.status[i]);
			writer.EndObject();
		}
		writer.EndArray();
		writer.EndObject();

		stringstream css;
		css << workingFolder << "/" << instantFileName << "-" << devId <<".txt";
		string path = css.str();

		
		FILE * pFile;
		pFile = fopen(path.c_str(), "w");
	 //fprintf(pFile, "%s", oss.str().c_str());
		fprintf(pFile, "%s", s.GetString());
		fclose(pFile);
	}

	static void writeTxt(PollData _dataToFile,int devId,int timebase){
		time_t now = time(0);
		long long timeInMinutes=long(now/60)-(long(now/60)%timebase);

		StringBuffer s;
		Writer<StringBuffer> writer(s);
		writer.StartObject(); 
		writer.Key("time");
		writer.Double(timeInMinutes*60); 
		writer.Key("channels");
		writer.StartArray();
		for (unsigned i = 0; i < _dataToFile.len; i++)
		{
			writer.StartObject();
			writer.Key("id");
			writer.Uint(i);
			writer.Key("value");

			ostringstream ss;
			ss << _dataToFile.values[i];
			string s(ss.str());

			writer.String(s.c_str());
			writer.Key("status");
			writer.Uint(_dataToFile.status[i]);
			writer.EndObject();
		}
		writer.EndArray();
		writer.EndObject();

		// stringstream oss;
		// oss << timeInMinutes<<",";

		// int var;
		// for ( var = 0;  var < _dataToFile.len; ++ var) {
		// 	oss<<_dataToFile.values[var]<<","<<_dataToFile.status[var]<<",";
		// }

		// oss<<"\r\n";

		stringstream css;
		css << workingFolder << "/" << dataFileName << "-" << devId <<".txt";
		string DataPath = css.str();
		
		stringstream cssCopy;
		css << workingFolder << "/" << dataCopyFileName << "-" << devId <<".txt";
		string DataPathCopy = css.str();


		bool fullFile  = isFileFull(DataPath);

		if(fullFile)
		{
			stringstream deleteFirstLine;
			deleteFirstLine<<"sed -n '2,$p' "<<DataPath<<" > "<<DataPathCopy;
			system(deleteFirstLine.str().c_str());
			deleteFirstLine.str(string());

			deleteFirstLine<<"mv "<<DataPathCopy<<" "<<DataPath;
			system(deleteFirstLine.str().c_str());

		}
		stringstream oss;
		oss<<s.GetString();
		oss<<"\r\n";
		FILE * pFile;
		pFile = fopen(DataPath.c_str(), "a");
		//fprintf(pFile, "%s", s.GetString());
		fprintf(pFile, "%s", oss.str().c_str());
		fclose(pFile);
	}

	static void writeTxtWithSummary(DataForSave _dataToFile,int devId,int timebase){
		time_t now = time(0);
		long long timeInMinutes=long(now/60)-(long(now/60)%timebase);

		StringBuffer s;
		Writer<StringBuffer> writer(s);
		writer.StartObject(); 
		writer.Key("time");
		writer.Double(timeInMinutes*60); 
		writer.Key("channels");
		writer.StartArray();
		for (unsigned i = 0; i < _dataToFile.len; i++)
		{
			writer.StartObject();
			writer.Key("id");
			writer.Uint(i);
			writer.Key("value");

			ostringstream ss;
			ss << _dataToFile.values[i];
			string s(ss.str());

			writer.String(s.c_str());
			writer.Key("status");
			writer.Uint(_dataToFile.status[i]);
			
			if(_dataToFile.summary[i].enable)
			{
				writer.Key("Summary");
				writer.StartObject();

				writer.Key("Maximum");
				ostringstream ssMax;
				if(_dataToFile.status[i]==0)
					ssMax <<-9999;
				else
					ssMax << _dataToFile.summary[i].max;
				string smax(ssMax.str());
				writer.String(smax.c_str());

				writer.Key("Minimum");
				ostringstream ssMin;
				if(_dataToFile.status[i]==0)
					ssMin <<-9999;
				else
					ssMin << _dataToFile.summary[i].min;
				string smin(ssMin.str());
				writer.String(smin.c_str());

				writer.Key("Std");
				ostringstream ssStd;
				if(_dataToFile.status[i]==0)
					ssStd <<-9999;
				else
					ssStd << _dataToFile.summary[i].std;
				string sStd(ssStd.str());
				writer.String(sStd.c_str());

				writer.EndObject();
			}

			writer.EndObject();
		}
		writer.EndArray();
		writer.EndObject();

		// stringstream oss;
		// oss << timeInMinutes<<",";

		// int var;
		// for ( var = 0;  var < _dataToFile.len; ++ var) {
		// 	oss<<_dataToFile.values[var]<<","<<_dataToFile.status[var]<<",";
		// }

		// oss<<"\r\n";

		stringstream css;
		css << workingFolder << "/" << dataFileName << "-" << devId <<".txt";
		string DataPath = css.str();
		
		stringstream cssCopy;
		css << workingFolder << "/" << dataCopyFileName << "-" << devId <<".txt";
		string DataPathCopy = css.str();


		bool fullFile  = isFileFull(DataPath);

		if(fullFile)
		{
			stringstream deleteFirstLine;
			deleteFirstLine<<"sed -n '2,$p' "<<DataPath<<" > "<<DataPathCopy;
			system(deleteFirstLine.str().c_str());
			deleteFirstLine.str(string());

			deleteFirstLine<<"mv "<<DataPathCopy<<" "<<DataPath;
			system(deleteFirstLine.str().c_str());

		}
		stringstream oss;
		oss<<s.GetString();
		oss<<"\r\n";
		FILE * pFile;
		pFile = fopen(DataPath.c_str(), "a");
		//fprintf(pFile, "%s", s.GetString());
		fprintf(pFile, "%s", oss.str().c_str());
		fclose(pFile);
	}
	
	private :
	static bool isFileFull(string path){
		FILE *fp = fopen(path.c_str(),"r");
		int ch=0;
		int lines=0;

		if (fp == NULL) return false;

		while(!feof(fp))
		{
			ch = fgetc(fp);
			if(ch == '\n')
			{
				lines++;
			}
		}

		
		return lines>=MaxRecordsInFile;
	} 
};


#endif /* DATAFILE_HPP_*/