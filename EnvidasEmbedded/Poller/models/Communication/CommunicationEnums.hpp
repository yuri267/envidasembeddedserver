#ifndef Communication_Enums_Hpp
#define Communication_Enums_Hpp

enum Baud
{
	baud50=50,
	baud75=75,
	baud110=110,
	baud134=134,
	baud150=150,
	baud200=200,
	baud300=300,
	baud600=600,
	baud1200=1200,
	baud1800=1800,
	baud2400=2400,
	baud4800=4800,
	baud9600=9600,
	baud19200=19200,
	baud38400=38400,
	baud57600=57600,
	baud115200=115200,
	baud230400=230400,
	baud460800=460800,
	baud500000=500000,
	baud576000=576000,
	baud921600=921600,
	baud1000000=1000000
};

#endif /* Communication_Enums_Hpp */