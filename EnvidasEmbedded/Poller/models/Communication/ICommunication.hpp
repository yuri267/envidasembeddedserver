#ifndef ICommunication_HPP_
#define ICommunication_HPP_

class ICommunication
{
public:
	
	virtual bool OpenConnection()=0;
	virtual bool CloseConnection()=0;
	virtual void Send(string message)=0;
	virtual void SendByte(unsigned char *message)=0;
	virtual void SendByte(unsigned char *message,int size)=0;
	virtual int ReceiveRawByte()=0;
	virtual int ReceiveRawByte(unsigned char *buf)=0;
 	virtual unsigned char* ReceiveByte()=0;

	void Wait(long Time)
	{
		usleep(Time);
	}
	
	void WaitInSeconds(int Time)
	{
		sleep(Time);
	}

	int maxLineLentgh()
	{
		return 10000;
	}
};


#endif /* ICommunication_HPP_ */