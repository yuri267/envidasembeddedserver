
#ifndef SerialPort_HPP_
#define SerialPort_HPP_

#include "ICommunication.hpp"
#include "rs232.hpp"

class serialPort:virtual public ICommunication
{

public:
	serialPort(){}
	
	serialPort(int port, int baudRate, int Databit, int stopBit, int parity, bool listener){
		this->parity=parity;
		this->baudRate=baudRate;
		this->port=port;
		this->databit=Databit;
		this->listener=listener;
		this->stopbit=stopBit;
		packetWait=800;
		threadSleep=500;
		waitData=1000;
	}

	bool OpenConnection(){
		int i;
		i=RS232_OpenComport(port,baudRate);
		if (i==0)
		{

			return true;
		}
		else
		{
			er.saveStringToBufferw("can't open the port");
			return false;
		}
	}
	
	bool CloseConnection(){
		RS232_CloseComport(port);
		return true;
	}

	void Send(string message){
		const	char* charTosend=message.c_str();
		RS232_cputs(port,charTosend);
	}

	void SendByte(unsigned char *message){
			int i=0;
			int size=0;
			while(message[i]!='\0') size++;
			RS232_SendBuf(port, message, size);
	}

	void SendByte(unsigned char *message,int size){
		RS232_SendBuf(port, message, size);
	}

	int ReceiveRawByte(){
		unsigned char *buf;
		int i=RS232_PollComport(port,buf,10000);
		return i;
	}

	int ReceiveRawByte(unsigned char *buf){
		int i=RS232_PollComport(port,buf,10000);
		return i;
	}

	unsigned char* ReceiveByte(){
		unsigned char *buf;
		RS232_PollComport(port,buf,10000);
		return buf;
	}


private: 

	int port;
	int baudRate;
	int databit;
	int stopbit;
	int parity;
	bool listener;
	int packetWait;
	int threadSleep;
	long waitData;
	int maxLineLentgh;
	errorLog er;
};

#endif /* SerialPort_HPP_ */