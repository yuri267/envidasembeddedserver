#ifndef RS232_HPP_
#define RS232_HPP_

#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include "CommunicationEnums.hpp"



int Cport[30];
int error;

struct termios new_port_settings,
old_port_settings[30];

char comports[30][16]={"/dev/ttyLP0","/dev/ttyLP2","/dev/ttyLP1","/dev/ttyS3","/dev/ttyS4","/dev/ttyS5",
		"/dev/ttyS6","/dev/ttyS7","/dev/ttyS8","/dev/ttyS9","/dev/ttyS10","/dev/ttyS11",
		"/dev/ttyS12","/dev/ttyS13","/dev/ttyS14","/dev/ttyS15","/dev/ttyUSB0",
		"/dev/ttyUSB1","/dev/ttyUSB2","/dev/ttyUSB3","/dev/ttyUSB4","/dev/ttyUSB5",
		"/dev/ttyAMA0","/dev/ttyAMA1","/dev/ttyACM0","/dev/ttyACM1",
		"/dev/rfcomm0","/dev/rfcomm1","/dev/ircomm0","/dev/ircomm1"};


	int RS232_OpenComport(int comport_number, int baudrate){
		int baudr, status;


		if((comport_number>29)||(comport_number<0))
		{
			return(1);
		}

		switch(baudrate)
		{
			case      50 : baudr = B50;
			break;
			case      75 : baudr = B75;
			break;
			case     110 : baudr = B110;
			break;
			case     134 : baudr = B134;
			break;
			case     150 : baudr = B150;
			break;
			case     200 : baudr = B200;
			break;
			case     300 : baudr = B300;
			break;
			case     600 : baudr = B600;
			break;
			case    1200 : baudr = B1200;
			break;
			case    1800 : baudr = B1800;
			break;
			case    2400 : baudr = B2400;
			break;
			case    4800 : baudr = B4800;
			break;
			case    9600 : baudr = B9600;
			break;
			case   19200 : baudr = B19200;
			break;
			case   38400 : baudr = B38400;
			break;
			case   57600 : baudr = B57600;
			break;
			case  115200 : baudr = B115200;
			break;
			case  230400 : baudr = B230400;
			break;
			case  460800 : baudr = B460800;
			break;
			case  500000 : baudr = B500000;
			break;
			case  576000 : baudr = B576000;
			break;
			case  921600 : baudr = B921600;
			break;
			case 1000000 : baudr = B1000000;
			break;
			default      : 
			return(1);
			break;
		}

		Cport[comport_number] = open(comports[comport_number], O_RDWR | O_NOCTTY | O_NDELAY);
		if(Cport[comport_number]==-1)
		{
			return(1);
		}

		error = tcgetattr(Cport[comport_number], old_port_settings + comport_number);
		if(error==-1)
		{
			close(Cport[comport_number]);
			return(1);
		}
		memset(&new_port_settings, 0, sizeof(new_port_settings));  /* clear the new struct */	


		new_port_settings.c_cflag = baudr | CS8 | CLOCAL | CREAD;
		new_port_settings.c_iflag = IGNPAR;
		new_port_settings.c_oflag = 0;
		new_port_settings.c_lflag = 0;
		new_port_settings.c_cc[VMIN] = 0;      /* block untill n bytes are received */
		new_port_settings.c_cc[VTIME] = 0;     /* block untill a timer expires (n * 100 mSec.) */
		error = tcsetattr(Cport[comport_number], TCSANOW, &new_port_settings);
		if(error==-1)
		{
			close(Cport[comport_number]);
			
			return(1);
		}

		if(ioctl(Cport[comport_number], TIOCMGET, &status) == -1)
		{
			return(1);
		}

		status |= TIOCM_DTR;    /* turn on DTR */
		status |= TIOCM_RTS;    /* turn on RTS */

		if(ioctl(Cport[comport_number], TIOCMSET, &status) == -1)
		{
			return(1);
		}

		return(0);
	}

	int RS232_PollComport(int comport_number, unsigned char *buf, int size){
		int n;

		n = read(Cport[comport_number], buf, size);

		return(n);
	}

	int RS232_SendByte(int comport_number, unsigned char byte) {
		int n;

		n = write(Cport[comport_number], &byte, 1);
		if(n<0)  return(1);

		return(0);
	}

	int RS232_SendBuf(int comport_number, unsigned char *buf, int size){
		return(write(Cport[comport_number], buf, size));
	}

	void RS232_CloseComport(int comport_number){
		int status;

		if(ioctl(Cport[comport_number], TIOCMGET, &status) == -1)
		{
			//er.saveStringToBufferw("unable to get portstatus on rs232");
		}

		status &= ~TIOCM_DTR;    /* turn off DTR */
		status &= ~TIOCM_RTS;    /* turn off RTS */

		if(ioctl(Cport[comport_number], TIOCMSET, &status) == -1)
		{
			//er.saveStringToBufferw("unable to set portstatus on rs232");
		}

		tcsetattr(Cport[comport_number], TCSANOW, old_port_settings + comport_number);
		close(Cport[comport_number]);
	}

	int RS232_IsDCDEnabled(int comport_number){
		int status;

		ioctl(Cport[comport_number], TIOCMGET, &status);

		if(status&TIOCM_CAR) return(1);
		else return(0);
	}

	int RS232_IsCTSEnabled(int comport_number) {
		int status;

		ioctl(Cport[comport_number], TIOCMGET, &status);

		if(status&TIOCM_CTS) return(1);
		else return(0);
	}

	int RS232_IsDSREnabled(int comport_number){
		int status;

		ioctl(Cport[comport_number], TIOCMGET, &status);

		if(status&TIOCM_DSR) return(1);
		else return(0);
	}

	void RS232_enableDTR(int comport_number){
		int status;

		if(ioctl(Cport[comport_number], TIOCMGET, &status) == -1)
		{
			//er.saveStringToBufferw("unable to get portstatus on rs232");
		}

		status |= TIOCM_DTR;    /* turn on DTR */

		if(ioctl(Cport[comport_number], TIOCMSET, &status) == -1)
		{
			//er.saveStringToBufferw("unable to set portstatus on rs232");
		}
	}

	void RS232_disableDTR(int comport_number){
		int status;

		if(ioctl(Cport[comport_number], TIOCMGET, &status) == -1)
		{
			//er.saveStringToBufferw("unable to get portstatus on rs232");
		}

		status &= ~TIOCM_DTR;    /* turn off DTR */

		if(ioctl(Cport[comport_number], TIOCMSET, &status) == -1)
		{
			//er.saveStringToBufferw("unable to set portstatus on rs232");
		}
	}

	void RS232_enableRTS(int comport_number){
		int status;

		if(ioctl(Cport[comport_number], TIOCMGET, &status) == -1)
		{
			//er.saveStringToBufferw("unable to get portstatus on rs232");
		}

		status |= TIOCM_RTS;    /* turn on RTS */

		if(ioctl(Cport[comport_number], TIOCMSET, &status) == -1)
		{
			//er.saveStringToBufferw("unable to set portstatus on rs232");
		}
	}

	void RS232_disableRTS(int comport_number){
		int status;

		if(ioctl(Cport[comport_number], TIOCMGET, &status) == -1)
		{
			//er.saveStringToBufferw("unable to get portstatus on rs232");
		}

		status &= ~TIOCM_RTS;    /* turn off RTS */

		if(ioctl(Cport[comport_number], TIOCMSET, &status) == -1)
		{
			//er.saveStringToBufferw("unable to set portstatus on rs232");
		}
	}

	void RS232_cputs(int comport_number, const char *text)  {
		while(*text != '\0')   
			RS232_SendByte(comport_number, *(text++));
	}

#endif /* RS232_HPP_ */