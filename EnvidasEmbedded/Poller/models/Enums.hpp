
#ifndef Enums_Hpp
#define Enums_Hpp

enum formula {
	noneFormula = 0 ,
	standardConditionFormula = 1
};

enum average {
	none = 0,
	mean = 1,
	windspeed = 2,
	winddirection = 3,
	sigma_Teta = 4,
	sum = 5,
	unit_Vector_Direction = 6,
	minimum = 7,
	maximum = 8,
	standardDeviation=9,
	yamartinoStandartDeviation = 10,

	standardCondition = 100
};

enum deviceEnum {
	d8017=0, 
	d8080=1, 
	rmY32500=2, 
	modbusc=3,
	TwoBOzone=4,
	virtualFormula=5,
	ES642Dust=6,
	d8050=7,
	d9017=8,
	d9051=9,
	d9056=10,
	d9051And6=11,
	dYSI600OMS=12,
	ZDCS4420=13,
	ADR1500=14
};

enum Pollutant {
	PM25=0,PM10=1,TSP=2
};

#endif /* Enums_Hpp */