
#ifndef DataStatus_HPP_
#define DataStatus_HPP_


using namespace std;

class DataStatus
{
public:
	enum dataStatus
	{
		Unknown = -1,
		NoData = 0,
		OK = 1,
		OffScan = 2,
		LessThenSample = 3,
		InValid = 4,
		Zero = 5,
		Span = 6,
		OUTCAL = 7,
		CommFail = 8,
		Calib = 9,
		ST_A = 10,
		ST_B = 11,
		ST_C = 12,
		ST_D = 13,
		ST_E = 14,
		ST_F = 15,
		PowerFail = 16,
		//Zerf = 17,
		Audit = 17,
		Alarm = 18,
		WarmUp = 19,
		StandBy = 20,
		Mnfld = 21,
		HeatLn = 22,
		O2Prob = 23,
		MIR_F = 24,
		SAM_F = 25,
		SEDbox = 26,
		Purge = 27,
		Maintain = 28,
		OverRange = 29,
		CalibFail = 30,
		Span1 = 31,
		UnderRange = 32,
		RateOfChange = 34,
		BadCondition = 35,
		Err_Protocol = 36,

		LessThenSampleCalib = 37,
		Span2 = 38,
		Span3 = 39,
		Span4 = 40,
		Span5 = 41,
		Span6 = 42,
		Span7 = 43,
		Span8 = 44,
		FormulaError = 45,
		Zerf = 76,
		OK_PwrFail = 77,
		OK_RS232 = 78,
		OK_BadCon = 79,
		OK_AboveR = 80,
		OK_BelowR = 81,
		OK_RateOC = 82,
		OK_CalibFail = 83,
		FileNotFound = 84,
		OK_Calib = 85,
		OK_LessThenSample = 86,
		Span9 = 87,
		Span10 = 88,
		OK_OffScan =89,
		Calm =90,
		OutOfConrol = 91,// OutOfConrol for fail in Caib
		Constant = 92
	};
	
};

#endif /* DataStatus_HPP_ */