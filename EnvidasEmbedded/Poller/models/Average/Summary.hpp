#ifndef Summary_HPP
#define Summary_HPP

struct Summary
{
	bool enable = false;
	float max = -9999;
	float min = 9999;	
	float std = 0;
};

#endif /* Summary_HPP */