#ifndef WindSpeed_Hpp
#define WindSpeed_Hpp

#include "AverageFactory.hpp"

class WindSpeed : public IAverage
{
public:

	AverageParams averageParams;

	WindSpeed(){}

	~WindSpeed(){}

	void resetAverage(){
		averageParams = AverageParams();
	}

	void Summarize(float value,int status,float* AddValues,int* AddStatus){
		double Rad;
		float minimum = AddValues[1];
		double direction = (double)AddValues[0];

		if (value >= minimum) {
			//direction = (double) directionValue;
			if (direction > 360)
				direction = direction - 360;
			Rad = toRadians(direction);
			averageParams.validSampleCount++;
			averageParams.fWDx += ((float) (sin(Rad)) * value); //x
			averageParams.fWDy += ((float) (cos(Rad)) * value); //y
		} else {
			//direction = (double) directionValue;
			if (direction > 360)
				direction = direction - 360;
			Rad = toRadians(direction);
			averageParams.validSampleCount++;
			averageParams.SigmaTfWDx += ((float) (sin(Rad)) * value); //x
			averageParams.SigmaTfWDy += ((float) (cos(Rad)) * value); //y
		}
	}
	
	AverageParams FinalizeAverage(){
			if (averageParams.validSampleCount > 0) {
				averageParams.fWDx /= (averageParams.validSampleCount + averageParams.validSampleCountMin);
				averageParams.fWDy /= (averageParams.validSampleCount + averageParams.validSampleCountMin);
				double SqrtVal = (double) (averageParams.fWDx * averageParams.fWDx)
						+ (double) (averageParams.fWDy * averageParams.fWDy); //  (double)(fAvg[1]*fAvg[1]) + (double)(fWDy*fWDy);
				averageParams.value = (float) sqrt(SqrtVal);
				averageParams.status = DataStatus::OK;
			} else if (averageParams.validSampleCountMin > 0) {
				averageParams.SigmaTfWDx /= (averageParams.validSampleCountMin);
				averageParams.SigmaTfWDy /= (averageParams.validSampleCountMin);
				double SqrtVal = (double) (averageParams.SigmaTfWDx * averageParams.SigmaTfWDx)
						+ (double) (averageParams.SigmaTfWDy * averageParams.SigmaTfWDy); //  (double)(fAvg[1]*fAvg[1]) + (double)(fWDy*fWDy);
				averageParams.value = (float) sqrt(SqrtVal);
				averageParams.status = DataStatus::OK;
			} else {
				averageParams.value = 0;
				averageParams.status = DataStatus::LessThenSample;
			}
		return averageParams;
	}

};

#endif /* WindSpeed_Hpp */