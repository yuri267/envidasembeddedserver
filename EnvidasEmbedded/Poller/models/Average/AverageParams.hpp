#ifndef AverageParams_HPP
#define AverageParams_HPP

#include "../PollData.hpp"
#include "Summary.hpp"

struct AverageParams{
	float sum=0;
	float value = 0;
	int status=0;
	int pollCount=0;
	int validSampleCount=0;
	int validSampleCountMin=0;
	float SigmaTfWDx=0.0f;
	float SigmaTfWDy=0.0f;
	float fWDy=0.0f;
	float fWDx=0.0f;
	double S=0;
	float pollWindSpeed=0.0f;
	float BiggestWindSpeed=0.0f;
	Summary summary;
};

#endif /* AverageParams_HPP */