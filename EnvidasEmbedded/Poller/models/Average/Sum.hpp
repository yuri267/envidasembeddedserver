#ifndef Sum_Hpp
#define Sum_Hpp

#include "AverageFactory.hpp"

class Sum : public IAverage
{
public:
	AverageParams averageParams;
	
	Sum(){lastCount=0;}

	Sum(bool summary){
		useSummary=summary;
		lastCount=0;
		averageParams.summary.enable=useSummary;
	}

	~Sum(){}
	
	void resetAverage(){
		averageParams = AverageParams();
		averageParams.summary.enable=useSummary;
		values.clear();
	}

	void Summarize(float value,int status,float* AddValues,int* AddStatus){
		averageParams.validSampleCount++;
		averageParams.sum = averageParams.sum + value-lastCount;
		lastCount = value;
		averageParams.status = status;

		if(useSummary && status==1)
		{
			if(value > averageParams.summary.max)
			{
				averageParams.summary.max = value;
			}

			if(value < averageParams.summary.min)
			{
				averageParams.summary.min = value;
			}

			values.push_back(value);	
		}

	}

	AverageParams FinalizeAverage(){
		if(averageParams.validSampleCount==0)
		{
			averageParams.value = -9999;
			averageParams.status = DataStatus::NoData;
		}
		else
		{	
			averageParams.value =  averageParams.sum;
		}
		return averageParams;
	}
private:
	float lastCount;
	vector<float> values;
	bool useSummary ;

	float calcSTD(){

		float mean=(float)averageParams.sum/averageParams.validSampleCount;
		float sum=0;
		for(int j=0;j<values.size();j++)
		{
			sum+=pow((values.at(j)-mean),2);
		}
		return (float)sum/averageParams.validSampleCount;
	}
};

#endif /* Sum_Hpp */