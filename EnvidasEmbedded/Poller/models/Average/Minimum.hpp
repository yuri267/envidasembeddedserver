#ifndef Minimum_Hpp
#define Minimum_Hpp

#include "AverageFactory.hpp"

class Minimum : public IAverage
{
public:
	AverageParams averageParams;

	Minimum(){}

	~Minimum(){}

	void resetAverage(){
		averageParams = AverageParams();
		averageParams.sum = 9999;
	}

	void Summarize(float value,int status,float* AddValues,int* AddStatus){
		averageParams.validSampleCount++;
		if(value < averageParams.sum)
		{
			averageParams.sum = value;
			averageParams.status = status;
		}
	}
	
	AverageParams FinalizeAverage(){
		if(averageParams.validSampleCount==0)
		{
			averageParams.value = -9999;
			averageParams.status = DataStatus::NoData;
		}
		else
		{
			averageParams.value =  averageParams.sum;
		}
		
		return averageParams;
	}
};

#endif /* Minimum_Hpp */