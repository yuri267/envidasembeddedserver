#ifndef YamartinoStandartDeviation_Hpp
#define YamartinoStandartDeviation_Hpp

#include "AverageFactory.hpp"

class YamartinoStandartDeviation : public IAverage
{
public:
	AverageParams averageParams;

	YamartinoStandartDeviation(){}
	
	~YamartinoStandartDeviation(){}
	
	void resetAverage(){
		averageParams = AverageParams();
	}

	void Summarize(float value,int status,float* AddValues,int* AddStatus){
		double direction = -9999;
		double currentDirection = -9999;
		float wdValue = AddValues[0];

		if (AddStatus[0]!= DataStatus::CommFail && wdValue != -9999) {
			direction = (double) wdValue;
			if (direction > 360)
				direction = direction - 360;
			currentDirection = toRadians(direction);
		}

		if (AddStatus[0] == DataStatus::OK)
		{
			if(averageParams.validSampleCountMin==0)
			{
				averageParams.validSampleCountMin=1 ;
			}

			if(wdValue!=-9999)
			{
				double Xtemp = ((float) (sin(currentDirection))); //*1
				double Ytemp = ((float) (cos(currentDirection))); //*1
				averageParams.fWDx += Xtemp;
				averageParams.fWDy += Ytemp;
				averageParams.validSampleCount++;
			}
		}
		else
		{
			if(averageParams.validSampleCountMin==0)
			{
				if(currentDirection!=-9999)
				{
					double Xtemp = ((float) (sin(currentDirection))); //*1
					double Ytemp = ((float) (cos(currentDirection))); //*1
					averageParams.fWDx += Xtemp;
					averageParams.fWDy += Ytemp;
				}

				averageParams.status = status;//set last status as avg status if we got no valid records
			}

		}

		if(currentDirection==-9999)
		{
			averageParams.pollCount++;
		}
	}

	AverageParams FinalizeAverage(){

		if(averageParams.validSampleCountMin==1)
		{
			averageParams.fWDx /= averageParams.validSampleCount;
			averageParams.fWDy /= averageParams.validSampleCount;
		}
		else
		{
			averageParams.fWDx /= averageParams.pollCount;
			averageParams.fWDy /= averageParams.pollCount;
		}

		double SqrtVal = (double) (averageParams.fWDx * averageParams.fWDx)
		+ (double) (averageParams.fWDy * averageParams.fWDy);


		try
		{
			double sValue = (double) sqrt(1- SqrtVal);

			if(!isnan_number(sValue))
			{

				double powVal = (double) pow(sValue,3);
				double powCalc = (0.1547 * powVal) + 1;
				double val = asin(sValue)*powCalc;

				averageParams.value = isnan_number(val) ? 0 : val;

			}
			else
				averageParams.value = 0;
		}
		catch (std::exception& e) {
			averageParams.value = 0;
		}

		if(averageParams.validSampleCountMin==1)
			averageParams.status = DataStatus::OK;

		return averageParams;
	}
};

#endif /* YamartinoStandartDeviation_Hpp */