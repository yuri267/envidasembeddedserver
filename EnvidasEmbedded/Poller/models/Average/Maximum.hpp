#ifndef Maximum_Hpp
#define Maximum_Hpp

#include "AverageFactory.hpp"

class Maximum : public IAverage
{
public:
	AverageParams averageParams;

	Maximum(){}

	~Maximum(){}
	
	void resetAverage(){
		averageParams = AverageParams();
		averageParams.sum = -9999;
	}

	void Summarize(float value,int status,float* AddValues,int* AddStatus){

		averageParams.validSampleCount++;
		if(value > averageParams.sum)
		{
			averageParams.sum = value;
			averageParams.status = status;
		}
	}

	AverageParams FinalizeAverage(){
		
		if(averageParams.validSampleCount==0)
		{
			averageParams.value = -9999;
			averageParams.status = DataStatus::NoData;
		}
		else
		{
			averageParams.value =  averageParams.sum;
		}
		
		return averageParams;
	}
};


#endif /* Maximum_Hpp */