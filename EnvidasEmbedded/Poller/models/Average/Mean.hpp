#ifndef Mean_Hpp
#define Mean_Hpp

#include "AverageFactory.hpp"


class Mean : public IAverage
{
public:
	
	AverageParams averageParams;

	
	Mean(){}
	
	Mean(bool summary){
		useSummary=summary;
		averageParams.summary.enable=useSummary;
	}

	~Mean(){}

	void resetAverage(){
		averageParams = AverageParams();
		averageParams.summary.enable=useSummary;
		values.clear();
	}

	void Summarize(float value,int status,float* AddValues,int* AddStatus){
		averageParams.validSampleCount++;
		averageParams.sum = averageParams.sum + value;
		averageParams.status = status;

		if(useSummary && status==1)
		{
			if(value > averageParams.summary.max)
			{
				averageParams.summary.max = value;
			}

			if(value < averageParams.summary.min)
			{
				averageParams.summary.min = value;
			}

			values.push_back(value);	
		}
		
	}

	AverageParams FinalizeAverage(){
		
		
		if(averageParams.validSampleCount==0)
		{
			averageParams.value = -9999;
			averageParams.status = DataStatus::NoData;
		}	
		else
		{
			if(useSummary)
			{
				averageParams.summary.std = calcSTD();
			}
			
			averageParams.value = (float) averageParams.sum/ averageParams.validSampleCount;
		}
		if(isnan_number(averageParams.value))
		{
			averageParams.value = -9999;
			averageParams.status = DataStatus::NoData;
		}

		return averageParams;
	}


private: 
	vector<float> values;
	bool useSummary ;

	float calcSTD(){
		if(values.size()==0) return 0;
		float mean=(float)averageParams.sum/averageParams.validSampleCount;
		float sum=0;
		for(int j=0;j<values.size();j++)
		{
			sum+=pow((values.at(j)-mean),2);
		}
		return (float)sum/averageParams.validSampleCount;
	}
};


#endif /* Mean_Hpp */