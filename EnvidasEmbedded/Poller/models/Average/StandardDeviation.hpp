#ifndef StandardDeviation_Hpp
#define StandardDeviation_Hpp

#include "AverageFactory.hpp"



class StandardDeviation : public IAverage
{
public:
	AverageParams averageParams;

	StandardDeviation(){}

	~StandardDeviation(){}
	
	void resetAverage(){
		averageParams = AverageParams();
		values.clear();
	}

	void Summarize(float value,int status,float* AddValues,int* AddStatus){	
		averageParams.sum = averageParams.sum + value;
		averageParams.status = status;

		if(status==1)
		{
			values.push_back(value);
		}
		
		averageParams.validSampleCount++;
	}

	AverageParams FinalizeAverage(){
		float mean=(float)averageParams.sum/averageParams.validSampleCount;
		float sum=0;
		for(int j=0;j<values.size();j++)
		{
			sum+=pow((values.at(j)-mean),2);
		}
		averageParams.value=(float)sum/averageParams.validSampleCount;
		return averageParams;
	}

private: 
	vector<float> values;
	int instanceReSize;
	int maxArrayLenght;
};
#endif /* StandardDeviation_Hpp */