#ifndef AverageFactory_HPP
#define AverageFactory_HPP

#include "AverageParams.hpp"


class IAverage
{
public:
	 virtual ~IAverage() {};
	 virtual void resetAverage() = 0;
	 virtual void Summarize(float value,int status,float* AddValues,int* AddStatus) = 0;
	 virtual AverageParams FinalizeAverage() = 0;
};

void resetsAverage(IAverage * obj) {
    obj->resetAverage();
}

void Summarize(IAverage * obj,float value,int status,float* AddValues,int* AddStatus) {
    obj->Summarize(value,status,AddValues,AddStatus);
}

AverageParams FinalizeAverage(IAverage * obj) {
    return obj->FinalizeAverage();
}

#endif /* AverageFactory_HPP */

