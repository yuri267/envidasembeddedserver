#ifndef WindDirection_Hpp
#define WindDirection_Hpp

#include "AverageFactory.hpp"

class WindDirection : public IAverage
{
public:
	
	AverageParams averageParams;

	WindDirection(){}
	
	~WindDirection(){}
	
	void resetAverage(){
		averageParams = AverageParams();
	}

	void Summarize(float value,int status,float* AddValues,int* AddStatus){
		
		double Rad;
		float exCH = AddValues[0];
		float minimum = AddValues[1];
		double direction = (double)value;
		int additionch;


		if (direction >= minimum) {

			if (direction > 360)
				direction = direction - 360;
			Rad = toRadians(direction);
			averageParams.validSampleCount++;
			averageParams.fWDx += (float) ((sin(Rad) * exCH)); //x
			averageParams.fWDy += (float) ((cos(Rad) * exCH)); //y
		} else {
			if (direction > 360)
				direction = direction - 360;
			Rad = toRadians(direction);
			averageParams.validSampleCountMin++;
			averageParams.SigmaTfWDx += (((float) (sin(Rad))) * exCH); //x
			averageParams.SigmaTfWDy += (((float) (cos(Rad))) * exCH); //y
		}
	}

	AverageParams FinalizeAverage(){
		if (averageParams.validSampleCount > 0) {
			averageParams.fWDx /= (averageParams.validSampleCount + averageParams.validSampleCountMin);
			averageParams.fWDy /= (averageParams.validSampleCount + averageParams.validSampleCountMin);
			double SqrtVal = (double) (averageParams.fWDx * averageParams.fWDx)
						+ (double) (averageParams.fWDy * averageParams.fWDy); //  (double)(fAvg[1]*fAvg[1]) + (double)(fWDy*fWDy);
						averageParams.value = (float) sqrt(SqrtVal);

						if (averageParams.value != 0) {
							double temp = asin((averageParams.fWDx / averageParams.value));
							averageParams.value = (float) toDegrees(temp);
						} else
						averageParams.value = 0;

						averageParams.status = DataStatus::OK;

						if (averageParams.fWDy < 0)
							averageParams.value = 180 - averageParams.value;
						else if ((averageParams.fWDy > 0) && (averageParams.fWDx < 0))
							averageParams.value = 360 + averageParams.value;
					}
					else if (averageParams.validSampleCountMin > 0) {
						averageParams.SigmaTfWDx /= (averageParams.validSampleCountMin);
						averageParams.SigmaTfWDy /= (averageParams.validSampleCountMin);
						double SqrtVal = (double) (averageParams.SigmaTfWDx * averageParams.SigmaTfWDx)
						+ (double) (averageParams.SigmaTfWDy * averageParams.SigmaTfWDy); //  (double)(fAvg[1]*fAvg[1]) + (double)(fWDy*fWDy);
						averageParams.value = (float) sqrt(SqrtVal);

						if (averageParams.value  != 0) {
							double temp = asin((averageParams.SigmaTfWDx / averageParams.value));
							averageParams.value = (float) toDegrees(temp);
						} else
						averageParams.value = 0;

						averageParams.status = DataStatus::OK;
						if (averageParams.SigmaTfWDy < 0)
							averageParams.value = 180 - averageParams.value;
						else if ((averageParams.SigmaTfWDy > 0) && (averageParams.SigmaTfWDx < 0))
							averageParams.value = 360 + averageParams.value;
					}
					else {
						averageParams.value = -9999;
						if(averageParams.status==DataStatus::OK)
							averageParams.status = DataStatus::NoData;
					}
					
					return averageParams;
				}
			};

#endif /* WindDirection_Hpp */