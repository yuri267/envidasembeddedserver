#ifndef SigmaTeta_Hpp
#define SigmaTeta_Hpp

#include "AverageFactory.hpp"

class SigmaTeta : public IAverage
{
public:
	AverageParams averageParams;

	SigmaTeta(){}
	
	~SigmaTeta(){}
	
	void resetAverage(){
		averageParams = AverageParams();
	}

	void Summarize(float value,int status,float* AddValues,int* AddStatus){
		double direction;
		double currentDirection = 0;
		float wsValue = AddValues[0];
		float wdValue = AddValues[1];
		float minimum = AddValues[2];

		int wsStatus = AddStatus[0];
		int wdStatus = AddStatus[1];

		if (wsStatus!= DataStatus::CommFail && wdStatus != DataStatus::CommFail&& wsValue != -9999 && wdValue!= -9999) {
			direction = (double) wdValue;
			if (direction > 360)
				direction = direction - 360;
			currentDirection = toRadians(direction);
		}

		if (wsValue >= minimum && wsStatus== DataStatus::OK) {
			averageParams.validSampleCount++;
			double Xtemp = ((float) (sin(currentDirection)) * wsValue);
			double Ytemp = ((float) (cos(currentDirection)) * wsValue);
			averageParams.fWDx += Xtemp;
			averageParams.fWDy += Ytemp;
			double SqrtVal = (double) (Xtemp * Xtemp)
					+ (double) (Ytemp * Ytemp); //  (double)(fAvg[1]*fAvg[1]) + (double)(fWDy*fWDy);
			averageParams.S += (float) sqrt(SqrtVal);

		} else {
			double Xtemp = ((float) (sin(currentDirection)) * wsValue);
			double Ytemp = ((float) (cos(currentDirection)) * wsValue);
			averageParams.fWDx += Xtemp;
			averageParams.fWDy += Ytemp;
			double SqrtVal = (double) (Xtemp * Xtemp)
					+ (double) (Ytemp * Ytemp); //  (double)(fAvg[1]*fAvg[1]) + (double)(fWDy*fWDy);
			averageParams.S += (float) sqrt(SqrtVal);
			if (wsValue < minimum) {
				averageParams.status = DataStatus::InValid;
			}
		}
	}

	AverageParams FinalizeAverage(){
		averageParams.fWDx /= averageParams.validSampleCount;
		averageParams.fWDy /= averageParams.validSampleCount;
		averageParams.S /= averageParams.validSampleCount;

		double SqrtVal = (double) (averageParams.fWDx * averageParams.fWDx)
					+ (double) (averageParams.fWDy * averageParams.fWDy); //  (double)(fAvg[1]*fAvg[1]) + (double)(fWDy*fWDy);
		double sValue = (double) sqrt(SqrtVal);

		double Sround = averageParams.S;
			if (Sround == 0) {
				averageParams.value = 0;
				averageParams.status = DataStatus::InValid;
			} else {
				double val = sValue / Sround;
				averageParams.value = 81 * (double) sqrt(1 - val);
				if(isnan_number(averageParams.value))
					averageParams.value = 0;
				averageParams.status = DataStatus::OK;
			}

		return averageParams;
	}

};

#endif /* SigmaTeta_Hpp */