#ifndef daq8080_Hpp
#define daq8080_Hpp

#include "ProtocolIncludes.hpp"

class daq8080 : public ProtocolFactory
{
public:
	
	daq8080(device dev){
		this->channelsd=dev.getChannels().size();
		this->baundRate=dev.getBaundRate();
		this->port=dev.getPort();
		this->devNum=dev.getType();
		this->address = dev.getAddress();

		this->counterInit = false;
		counterCurrent=new long[2];
		for (int i = 0; i < 2; ++i) {
			counterCurrent[i]=0;
		}
	}

	~daq8080(){}
	
	PollData start(){return poll();}

	string Name(){return "DAC 8080";}

private:

	int delay;
	int port;
	int devNum;
	int address;
	errorLog elog;
	int channelsd;
	int baundRate;
	long *counterCurrent;
	bool counterInit;
	long counterMax[2];

	PollData poll(){
		PollData data;
		data.ReSize(channelsd);
		stringstream sendStream;
		string rev="";

		if (!counterInit)//first read
		{
			//zero counters
			for (int i = 0; i < channelsd; i++)
			{
				sendStream.str("");
				if(address<10)
					sendStream << "$0"<<address<<"6" << i << "\r";
				else
					sendStream << "$"<<address<<"6" << i << "\r";
				rev=getData(sendStream.str());



				if ((signed)rev.find("02",0) > -1 && rev.length()>0)
				{
					continue;
				}
				else
				{
					elog.saveStringToBufferw("Cannot init the counter");
					//cout<<"Cannot init the counter"<<endl;
					PollData d;
					return d;
				}

			}


			//get counter threashold

			for (int i = 0; i < channelsd; i++)
			{
				sendStream.str("");
				if(address<10)
					sendStream << "$0"<<address<<"3" << i << "\r";
				else
					sendStream << "$"<<address<<"3" << i << "\r";
				rev=getData(sendStream.str());
				if ( rev.length() > 0 && rev.at(0)== '!')
				{
					std::istringstream iss(rev.substr(3,8));
					iss >> std::hex >> counterMax[i];
				}
				else
				{
					elog.saveStringToBufferw("Cannot get the counterMax from the counter");
					//	cout<<"Cannot get the counterMax from the counter"<<endl;
					PollData d;
					return d; //return DataStatus.CommFail;
				}
			}
			counterInit = true;
		}

		for (int i = 0; i < channelsd; i++)
		{
			data.status[i]=DataStatus::CommFail;              //data[i].status = DataStatus.CommFail;
		}


		for (int i = 0; i < channelsd; i++)
		{
			sendStream.str("");
			if(address<10)
				sendStream << "#0"<<address<<i << "\r";
			else
				sendStream << "#"<<address <<i<< "\r";
			rev=getData(sendStream.str());

			//cout<<rev<<endl;

			try{
				if ( rev.length() > 0 && rev[0] == '>')
				{
					long val = -1;

					std::istringstream iss(rev.substr(1,8));
					iss >> std::hex >> val;
					if(val>=counterMax[i])
					{
						//zero counter
						sendStream.str("");
						if(address<10)
							sendStream << "$0"<<address<<"6" << i << "\r";
						else
							sendStream << "$"<<address<<"6" << i << "\r";
						rev=getData(sendStream.str());

						if ((signed)rev.find("02",0) > -1 && rev.length()>0)

							continue;

						else
						{
							//return -1;//return DataStatus.CommFail;
						}
					}
					if (val == -1)
					{
						data.values[i]=0;
						data.status[i]=DataStatus::CommFail;//DataStatus.CommFail
					}
					else
					{

						data.values[i]=(double)(val - this->counterCurrent[i]);
						this->counterCurrent[i] = val;
						data.status[i]=DataStatus::OK;


					}
				}
				else
				{
					data.values[i] = 0;
					data.status[i] = DataStatus::InValid;//DataStatus.InValid;
				}
			}
			catch (std::exception& e) {
				data.values[i]=0;
				data.status[i]=DataStatus::CommFail;//DataStatus.CommFail
			}
		}
		return data;
	}

	string getData(string send){
		string line = "";
		ICommunication * sp = new serialPort(port, this->baundRate, 1, 1, 1, false);
		bool open = sp->OpenConnection();
		if (!open)
			elog.saveStringToBufferw("cannot open the port");
		else {

			sp->Send(send);
			sp->Wait(100000);

			int i;
			unsigned char buf[sp->maxLineLentgh()];

			if ((i = sp->ReceiveRawByte(buf)) > 0) {
				line = line + charArrayTostring(buf, i);

			}
			sp->CloseConnection();

		}

		return line;
	}

};

#endif /* daq8080_Hpp */