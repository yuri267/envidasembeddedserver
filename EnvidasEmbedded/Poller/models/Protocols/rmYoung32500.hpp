#ifndef RMYOUNG_HPP_
#define RMYOUNG_HPP_

#include "ProtocolIncludes.hpp"

class rmYoung32500 : public ProtocolFactory
{
public:

	rmYoung32500(device dev){
		this->channelsd=dev.getChannels().size();
		this->baundRate=dev.getBaundRate();
		this->port=dev.getPort();
		this->devNum=dev.getType();
		this->address = dev.getAddress();
		this->calib = 0;
		this->status=DataStatus::OK;
		string tmp = dev.getAdditionData();
		if(tmp.length()>0)
			parseAdditionData(dev.getAdditionData());
	}

	~rmYoung32500(){}

	PollData start(){return poll();}

	string Name(){return "rmYoung 32500";}

private :

	int delay;
	int port;
	int devNum;
	int address;
	errorLog elog;
	int channelsd;

	int baundRate;
	bool calib;
	int status;

	void parseAdditionData(string data){
		int pos  = data.find("status=");

		if(pos>-1)
		{

			try
			{
				int ind = data.find(",",pos+7);
				string num = data.substr(pos+7,ind-pos-7);
				status = atoi(num.c_str());
			}
			catch(std::exception& e)
			{
				status = DataStatus::OK;
			}
		}
		else
		{
			status = DataStatus::OK;
		}

		pos  = data.find("calib=");

		if(pos>-1)
		{
			try
			{
				int ind = data.find(",",pos+6);
				string num = data.substr(pos+6,ind-pos-6);
				int bit = atoi(num.c_str());

				calib = (bit==1)?true:false;
			}
			catch(std::exception& e)
			{
				calib = false;
			}
		}
		else
		{
			calib = false;
		}
	}

	PollData poll(){
		//Ascii mode
		if(address>8) address = 0;
		PollData data;

		if(calib)
		{
			data.ReSize(channelsd);
			data.SetStatusToall(status);
			data.SetValueToAll(0);
			sleep(1);
			return data;
		}

		stringstream sendStream;
		sendStream <<"M"<<address<<"!";

		string send = sendStream.str();
		string line = getData(send);

		cout<<line<<endl;

		data=parseData(line);
		return data;
	}

	string getData(string send) {
		string line = "";
		ICommunication * sp = new serialPort(port, this->baundRate, 1, 1, 1, false);
		bool open = sp->OpenConnection();
		if (!open)
			elog.saveStringToBufferw("cannot open the port");
		else {

			sp->Send(send);
			sp->WaitInSeconds(1);

			int i;
			unsigned char buf[sp->maxLineLentgh()];

			if ((i = sp->ReceiveRawByte(buf)) > 0) {
				line = line + charArrayTostring(buf, i);

			}
			sp->CloseConnection();
		}

		return line;
	}

	PollData parseData(string data){
		//int WindSpeedType=5103;

		string str;
		PollData pd;
		pd.ReSize(channelsd);

		try{
			std::string clearData="";

			try{
				int ind  = data.find('\r',0);

				if(ind == -1) return pd;

				clearData = data.substr(0,ind);
			}
			catch(std::exception& e) {
				return pd;
			}

			std::vector<std::string> splitD = split(clearData.c_str(), ' ');
			std::string *assd= new std::string[channelsd];
			int y=0;

			for(int k=1;(unsigned)k<splitD.size();k++)
			{
				int in = splitD[k].find('*',0);

				//cout<<splitD[k]<<endl;

				if(splitD[k].length()!=0 && splitD[k].compare(" ")!=0 && y<channelsd && !in>=0)
				{
					assd[y++]=splitD[k];
				}
			}


			for (int var = 0; var < channelsd; ++var)
			{
				pd.values.at(var) = atof(assd[var].c_str());
				pd.status.at(var) = DataStatus::OK;
			}



		}
		catch (std::exception& e) {
			elog.saveStringToBufferw("wrong Device address/id or unconnected Device");
			pd.SetValueToAll(-9999);
			pd.SetStatusToall(4);

		}
		pd.SetStatusToall(status);
		return pd;
	}
};

#endif /* RMYOUNG_HPP_ */