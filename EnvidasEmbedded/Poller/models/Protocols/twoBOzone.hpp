#ifndef TWOBOZONE_HPP_
#define TWOBOZONE_HPP_

#include "ProtocolIncludes.hpp"

class twoBOzone : public ProtocolFactory
{
public:

	twoBOzone(device dev){
		this->channelsd=dev.getChannels().size();
		this->baundRate=dev.getBaundRate();
		this->port=dev.getPort();
		this->devNum=dev.getType();
		this->address = dev.getAddress();
	}

	~twoBOzone(){}

	PollData start(){return poll();}

	string Name(){return "TwoBOzone";}

private:
	int delay;
	int port;
	int devNum;
	int address;
	errorLog elog;
	int channelsd;
	int baundRate;

	PollData poll(){
		PollData _data;
		string line = getData("l");

		if(line.length()<15)//wrong data
		{
			_data.ReSize(channelsd);
			_data.SetStatusToall(4);
			return _data;
		}

		string saveLine;
		if(line.length()>20)
		{
			saveLine=line;
		}

		_data = parseData(saveLine);

		return _data;
	}

	string getData(string send){
		string line = "";
		ICommunication * sp = new serialPort(port, this->baundRate, 1, 1, 1, false);
		bool open = sp->OpenConnection();
		if (!open)
			elog.saveStringToBufferw("cannot open the port");
		else {
			sp->Send(send);
			sp->Wait(2000000);
			//sleep(2);
			int i;
			unsigned char buf[sp->maxLineLentgh()];

			if ((i = sp->ReceiveRawByte(buf)) > 0) {
				line = line + charArrayTostring(buf, i);

			}
			//cout<<line;

			sp->CloseConnection();

		}

		return line;
	}

	PollData parseData(string data) {

			PollData _data; //data+status
			_data.ReSize(channelsd);


			int index = 2;
			int indexEnd;
			int i;

			if(data.length()==0)
			{
				_data.ReSize(channelsd);
				_data.SetStatusToall(4);
			}
			else
			{
				if(data.at(0)=='L')
					index = 19;
				try{
					for (i = 0; i < channelsd; ++i) {
						indexEnd = data.find(",",index);
						try{
							_data.values[i] = (atof(data.substr(index,indexEnd-index).c_str()));
							_data.status[i] = DataStatus::OK;
						}
						catch (std::exception& e) {
							_data.status[i] = DataStatus::CommFail;
						}
						index=indexEnd+1;

					}
				}
				catch (std::exception& e) {

				}
			}
			return _data;
		}
	};

#endif /* TWOBOZONE_HPP_ */