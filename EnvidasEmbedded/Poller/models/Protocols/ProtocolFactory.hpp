#ifndef ProtocolFactory_HPP_
#define ProtocolFactory_HPP_

class ProtocolFactory
{
public:

    static ProtocolFactory * make_protocol(device dev);
    virtual ~ProtocolFactory(){}
    virtual PollData start() = 0;
	virtual string Name() = 0;
};



#endif /* ProtocolFactory_HPP_ */