#ifndef zdcs4420_Hpp
#define zdcs4420_Hpp

#include "ProtocolIncludes.hpp"


class zdcs4420 : public ProtocolFactory
{
public:
	zdcs4420(device dev){
		this->channelsd=dev.getChannels().size();
		this->baundRate=dev.getBaundRate();
		this->port=dev.getPort();
		this->devNum=dev.getType();
		this->address = dev.getAddress();
	}

	~zdcs4420(){}

	PollData start(){return poll();}

	string Name(){return "zdcs 4420";}

private:
	int delay;
	int port;
	int devNum;
	int address;
	errorLog elog;
	int channelsd;
	int baundRate;

	PollData poll (){
		PollData data;
		string line = getData();
		data=parseData(line);
		return data;
	}

	string getData(){
		string line = "";
		ICommunication * sp = new serialPort(port, this->baundRate, 1, 1, 1, false);
		bool open = sp->OpenConnection();
		if (!open)
			elog.saveStringToBufferw("cannot open the port");
		else {
			sp->Send("do sample\r\n");
			sp->Wait(1000000);
			int i;
			unsigned char buf[1000];

			if ((i = sp->ReceiveRawByte(buf)) > 0) {
				line = line + charArrayTostring(buf, i);

			}
			sp->CloseConnection();

		}

		return line;
	}

	PollData parseData(string data){
		PollData pd;
		pd.ReSize(channelsd);
		if(data.find("MEASUREMENT",0)<0)
		{
			pd.SetValueToAll(0);
			pd.SetStatusToall(4);
			return pd;
		}
		try
		{
			int first = data.find("MEASUREMENT",0);
			string linesub = data.substr(first+11,data.length()-first-11);

			std::vector<std::string> splitD = split(linesub.c_str(), '\t');
			std::string *assd= new std::string[channelsd+2+17];
			int y=0;
			int k=0;


			for(k=0;(unsigned)k<splitD.size();k++)
			{
				if(splitD[k].length()!=0 &&(splitD[k].find("(",0)<0 || splitD[k].find("(",0)>=splitD[k].length()) && (splitD[k].find("Ping",0)<0 || splitD[k].find("Ping",0)>=splitD[k].length())&& (splitD[k].find("\t",0)<0 || splitD[k].find("\t",0)>=splitD[k].length()))
				{
					assd[y++]=splitD[k];
				}
			}

			int var = 0;
			for (var = 2; var < channelsd+2; ++var) {
				pd.values.at(var-2) = atof(assd[var].c_str());
				pd.status.at(var-2) = DataStatus::OK;
			}

			return pd;
		}
		catch(std::exception& e)
		{
			pd.SetValueToAll(0);
			pd.SetStatusToall(4);
			return pd;
		}
		return pd;

	}
};


#endif /* zdcs4420_Hpp */