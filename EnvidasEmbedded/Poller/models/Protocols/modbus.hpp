#ifndef modbusc_Hpp
#define modbusc_Hpp

#include "ProtocolIncludes.hpp"
#include "stdint.h"

const int byteCountModebus=8;

class modbus : public ProtocolFactory
{
public:

	modbus(device dev){
		this->channelsd=dev.getChannels().size();
		this->address=dev.getAddress();
		this->port=dev.getPort();
		this->addData=dev.getAdditionData();
		this->baundRate=dev.getBaundRate();
		this->calib = 0;
		this->status=DataStatus::OK;


		
		int pos  = this->addData.find("status=");

		if(pos>-1)
		{

			try
			{
				int ind = this->addData.find(",",pos+7);
				string num = this->addData.substr(pos+7,ind-pos-7);
				status = atoi(num.c_str());
			}
			catch(std::exception& e)
			{
				status = DataStatus::OK;
			}
		}
		else
		{
			status = DataStatus::OK;
		}

		pos  = this->addData.find("calib=");

		if(pos>-1)
		{
			try
			{
				int ind = this->addData.find(",",pos+6);
				string num = this->addData.substr(pos+6,ind-pos-6);
				int bit = atoi(num.c_str());

				calib = (bit==1)?true:false;
			}
			catch(std::exception& e)
			{
				calib = false;
			}
		}
		else
		{
			calib = false;
		}
	}

	~modbus(){}

	PollData start(){	
		PollData data;
		modbusconfigFromAdditionData(additionDataArray);
		data=getDataFromDevice();
		return data;
	}

   	string Name(){return "modbus";}

private:
	int delay;
	int port;
	int deviceNo;
	int address;
	int channelsd;
	string addData;
	ICommunication * sp;
	int additionDataArray[6];
	//arr[0]-function,arr[1]-hi Address, arr[2]-lo Address, arr[3]-number of points hi, arr[4]- number of points low, arr[5]-variable type
	errorLog er;
	int baundRate;

	bool calib ;
	int status ;

	void modbusconfigFromAdditionData(int* arr) {
		
		int index = 0;
		int indexEnd;
		indexEnd = this->addData.find(",", index);
		arr[0]=atoi(this->addData.substr(index,indexEnd-index).c_str());
		index=indexEnd+1;
		indexEnd = this->addData.find(",", index);
		arr[1]=atoi(this->addData.substr(index,indexEnd-index).c_str());
		index=indexEnd+1;
		indexEnd = this->addData.find(",", index);
		arr[2]=atoi(this->addData.substr(index,indexEnd-index).c_str());
		index=indexEnd+1;
		indexEnd = this->addData.find(",", index);
		arr[3]=atoi(this->addData.substr(index,indexEnd-index).c_str());
		index=indexEnd+1;
		indexEnd = this->addData.find(",", index);
		arr[4]=atoi(this->addData.substr(index,indexEnd-index).c_str());
		index=indexEnd+1;
		indexEnd = this->addData.find(",", index);
		arr[5]=atoi(this->addData.substr(index,indexEnd-index).c_str());
	}

	string stringBuildToModeBus(){
		stringstream ss;

		if(this->address<16)
		{
			ss<<"0";
		}
		ss<<std::hex<<this->address;
		if(this->additionDataArray[0]<16)//additionDataArray[0]=function
		{
			ss<<"0";
		}
		ss<<std::hex<<this->additionDataArray[0];
		if(this->additionDataArray[1]<16)//additionDataArray[1]=hi Address
		{
			ss<<"0";
		}
		ss<<std::hex<<this->additionDataArray[1];
		if(this->additionDataArray[2]<16)//additionDataArray[1]=lo Address
		{
			ss<<"0";
		}
		ss<<std::hex<<this->additionDataArray[2];
		if(this->additionDataArray[3]<16)//additionDataArray[1]=number of points hi
		{
			ss<<"0";
		}
		ss<<std::hex<<this->additionDataArray[3];
		if(this->additionDataArray[4]<16)//additionDataArray[1]=number of points lo
		{
			ss<<"0";
		}
		ss<<std::hex<<this->additionDataArray[4];

		return ss.str();
	}

	string char_to_hex(const char *c ){
		std::stringstream stream;
		stream << "0x"<< std::hex << c;
		return stream.str();
	}

	u_int32_t ModRTU_CRC(unsigned char *buf, int len) {
		int pos,i;
		u_int32_t crc = 0xFFFF;

		for ( pos = 0; pos < len; pos++) {
			crc ^= (u_int32_t)buf[pos];          // XOR byte into least sig. byte of crc

			for ( i = 8; i != 0; i--) {    // Loop over each bit
				if ((crc & 0x0001) != 0) {      // If the LSB is set
					crc >>= 1;                    // Shift right and XOR 0xA001
					crc ^= 0xA001;
				}
				else                            // Else LSB is not set
					crc >>= 1;                    // Just shift right
			}
		}
		// Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
		return crc;
	}

	int checkCrc(unsigned char *s, int len){
		try{
			cout<<"checkCrc start 1" <<endl;
			u_int32_t l=ModRTU_CRC(s,len-2);
			int crc1,crc2;
			std::string str1,str2;
			cout<<"checkCrc start 2" <<endl;
			std::stringstream ss;
			ss<<std::hex<<l;
			str1=ss.str();

			cout<<"checkCrc start 3" <<endl;
			ss.str("");

			crc1=(int)s[len-2];
			crc2=(int)s[len-1];


			if(crc1<16)
				ss<<std::hex<<crc2<<"0"<<std::hex<<crc1;
			else
				ss<<std::hex<<crc2<<std::hex<<crc1;
			str2=ss.str();



			if(str1.at(0)=='0'&&str1.at(1)==str2.at(0))
			{
				str1=str1.substr(1,2);
			}
			else if(str2.at(0)=='0'&& str2.length()>2 &&str2.at(1)=='0' &&str2.at(2)==str1.at(0))
			{
				str2=str2.substr(2,1);
			}
			else if(str2.at(0)=='0'&&str2.at(1)==str1.at(0))
			{
				str2=str2.substr(1,2);
			}

			if(str1.compare(str2.c_str()))
			{
				stringstream oss;
				oss<<"error in the crc: "<<str1<<" & "<<str2;
				//er.saveStringToBufferw(oss.str());
			}

			return str1.compare(str2.c_str());//0-same crc
		}
		catch (std::exception& e) {
			return -1;
		}

		return -1;
	}

	void crcAddSend(std::string toSend,int len){
		unsigned char CMNDarray[len+2];
		char hex[2];
		int d;
		unsigned char c;
		int i,j;
		for (i = 0,j=0; i < len*2; ++j, i=i+2) {
			hex[0]=toSend.at(i); hex[1]=toSend.at(i+1);
			sscanf(hex,"%02x", &d);
			c=(unsigned char)d;
			CMNDarray[j]=c;
		}


		u_int32_t l;
		l=ModRTU_CRC(CMNDarray,6);
		std::stringstream ss;
		std::string str;
		ss<<std::hex<<l;
		if(ss.str().length()==3)
		{
			ss.str("");
			ss<<"0"<<std::hex<<l;
		}
		str=ss.str();


		hex[0]=str.at(2); hex[1]=str.at(3);
		sscanf(hex,"%02x", &d);
		c=(unsigned char)d;
		CMNDarray[6]=c;

		hex[0]=str.at(0); hex[1]=str.at(1);
		sscanf(hex,"%02x", &d);
		c=(unsigned char)d;
		CMNDarray[7]=c;


		
		sp->OpenConnection();
		sp->SendByte(CMNDarray,byteCountModebus);
		sp->Wait(100000);
	}

	string DecToBin(int number){
		if ( number == 0 ) return "0";
		if ( number == 1 ) return "1";

		if ( number % 2 == 0 )
			return DecToBin(number / 2) + "0";
		else
			return DecToBin(number / 2) + "1";
	}

	PollData ReadCoilStatus(unsigned char *s, int len,int ch){
		PollData dat;
		dat.ReSize(ch);
		int data=(int)s[3];
		std::string binData;
		binData=DecToBin(data);
		int i=0;
		try
		{
			for (i = 0; i < (signed)binData.length(); ++i) {
				dat.values[i]=binData.at(binData.length()-1-i)-48;
				dat.status[i]=DataStatus::OK;
			}
		}
		catch(std::exception& e)
		{
			dat.status[i]=DataStatus::InValid;
		}
		return dat;
	}

	PollData ReadInputStatus(unsigned char *s, int len,int ch){
		PollData dat;
		dat.ReSize(ch);
		int i=0;
		for (i = 0; i <ch; ++i) {
			dat.values[i]=0;
		}
		try
		{
			int data=(int)s[3];
			std::string binData;
			binData=DecToBin(data);

			for (i = 0; i < (signed)binData.length(); ++i) {
				dat.values[i]=binData.at(binData.length()-1-i)-48;
				dat.status[i]=DataStatus::OK;
			}

		}
		catch(std::exception& e)
		{
			dat.status[i]=DataStatus::InValid;
		}
		return dat;
	}

	PollData ReadInputRegisters(unsigned char *s, int len,int ch,int type){
		PollData data;
		switch(type)
		{
			case 1:
			data=signedType(s,len,ch);
			break;
			case 2: data=unsignedType(s,len,ch);
			break;
			case 3: data=longType(s,len,ch);
			break;
			case 4: data=longInverseType(s,len,ch);
			break;
			case 5: data=floatType(s,len,ch);
			break;
			case 6:	data=floatInverseType(s,len,ch);
			break;
			case 7: data=doubleType(s,len,ch);
			break;
			case 8: data=doubleInverseType(s,len,ch);
			break;
			default:
			break;
		}
		return data;
	}

	PollData ReadHoldingRegisters(unsigned char *s, int len,int ch,int type){
		PollData data;
		switch(type)
		{
			case 1: data=signedType(s,len,ch);
			break;
			case 2: data=unsignedType(s,len,ch);
			break;
			case 3: data=longType(s,len,ch);
			break;
			case 4: data=longInverseType(s,len,ch);
			break;
			case 5: data=floatType(s,len,ch);
			break;
			case 6:	data=floatInverseType(s,len,ch);
			break;
			case 7: data=doubleType(s,len,ch);
			break;
			case 8: data=doubleInverseType(s,len,ch);
			break;
			default:
				break;
		}
		return data;
	}

	PollData readDAtaModbus(unsigned char *s, int len,int ch){
		PollData data;
		int function=(int)s[1];
		switch(function)
		{
			case 1: data=ReadCoilStatus(s,len,ch);
			break;
			case 2:	data=ReadInputStatus(s,len,ch);
			break;
			case 3: data=ReadHoldingRegisters(s,len,ch,additionDataArray[5]);//1-signed, 2-unsigned, 3-long , 4-longInverse, 5-float, 6-floatInverse 7-double , 8-doubleInverse
			break;
			case 4: data=ReadInputRegisters(s,len,ch,additionDataArray[5]);
			break;
			default:
				break;
		}
		return data;
	}

	string buildData(int *a,int n){
		int i;
		stringstream ss;
		for (i = 0; i < n; ++i) {
			if(a[i]<16)
				ss<<"0";
			ss<<std::hex<<a[i];
		}
		return ss.str();
	}

	uint16_t getNum(unsigned char *s){
		union {
			uint16_t value;
			unsigned char bytes[byteCountModebus/4];
		} u;
		int i;
		for (i = 0; i < (signed)byteCountModebus/4;  ++i) {
			{
				u.bytes[(byteCountModebus/4)-1-i]=s[i];
			}
		}
		return u.value;
	}

	long getlong(unsigned char *s){
		union {
			long value;
			unsigned char bytes[byteCountModebus/2];
		} u;
		int i;
		for (i = 0; i < (signed)byteCountModebus/2;  ++i) {
			{
				u.bytes[byteCountModebus/2-1-i]=s[i];
			}
		}
		return u.value;
	}

	float getFloat(unsigned char *s){
		union {
			float value;
			unsigned char bytes[byteCountModebus/2];
		} u;
		int i;
		for (i = 0; i < (signed)byteCountModebus/2;  ++i) {
			{
				u.bytes[byteCountModebus/2-1-i]=s[i];
			}
		}
		return u.value;
	}

	double getDouble(unsigned char *s){
		union {
			double value;
			unsigned char bytes[byteCountModebus];
		} u;

		int i;
		for (i = 0; i < (signed)byteCountModebus;  ++i) {
			{
				u.bytes[byteCountModebus-1-i]=s[i];
			}
		}

		return u.value;
	}

	PollData getDataFromDevice() {
		PollData _data;

		_data.ReSize(channelsd);

		if(calib)
		{
			_data.initZeroValue(channelsd);
			_data.SetStatusToall(status);
			_data.SetValueToAll(0);
			sleep(1);
			return _data;
		}


		stringstream ss;
		string toSend=stringBuildToModeBus();

		sp = new serialPort(port, this->baundRate, 1, 1, 1, false);

		int len=toSend.length()/2;
		crcAddSend(toSend,len);

		 unsigned char * s;
		 int i = sp->ReceiveRawByte(s);
		 sp->CloseConnection();

		 cout<<"afrer ReceiveRawByte" << i <<endl;
		 
		 if(i>0 && checkCrc(s,i)==0)//check sum is good
		 {
		 	cout<<"afrer checkCrc" <<endl;
		 	try{
		 		_data=readDAtaModbus(s,i,channelsd);

		 		 cout<<"afrer readDAtaModbus" <<endl;

		 		_data.SetStatusToall(status);
		 	}
		 	catch (std::exception& e) {
		 		_data.commFailStatusToall();
		 	}
		 }
		 else
		 {
			_data.commFailStatusToall();
		 }

		return _data;
	}

	PollData signedType(unsigned char *s,int len,int ch){
		std::string str1;
		int i,j;
		long n;
		PollData data;
		data.ReSize(ch);
		unsigned char temp[byteCountModebus/4];

		for (i = 0,j=0; i < ch; ++i,j=j+byteCountModebus/4) {
			int k;
			for (k = 0; k < byteCountModebus/4; ++k) {

				if(k%2==0)
				{
					temp[k]=s[len-3-(j+k+1)];
				}
				else
				{
					temp[k]=s[len-3-(j+k-1)];
				}
			}

			try
			{
				n=getNum(temp);
				data.values[ch-1-i]=n;
				data.status[ch-1-i]=DataStatus::OK;
			}
			catch(std::exception& e)
			{
				data.status[ch-1-i]=DataStatus::InValid;
			}

		}

		return data;
	}

	PollData unsignedType(unsigned char *s,int len,int ch){
		std::string str1;
		int i,j;
		long n;
		PollData data;
		data.ReSize(ch);
		unsigned char temp[byteCountModebus/4];

		for (i = 0,j=0; i < ch; ++i,j=j+byteCountModebus/4) {
			int k;
			for (k = 0; k < byteCountModebus/4; ++k) {
				if(k%2==0)
				{
					temp[k]=s[len-3-(j+k+1)];
				}
				else
				{
					temp[k]=s[len-3-(j+k-1)];
				}
			}

			try
			{
				n=getNum(temp);
				data.values[ch-1-i]=n;
				data.status[ch-1-i]=DataStatus::OK;
			}
			catch(std::exception& e)
			{
				data.status[ch-1-i]=DataStatus::InValid;
			}
		}


		return data;
	}

	PollData longType(unsigned char *s,int len,int ch){
		std::string str1;
		int i=0,j=0;
		long n;
		PollData data;
		data.ReSize(ch);
		unsigned char temp[byteCountModebus/2];
		for (i = 0,j=0; i < ch; ++i,j=j+byteCountModebus/2) { //ch/2 - every channel is 2 bytes
			int k;
			for (k = 0; k < byteCountModebus/2; ++k) {
				if(k%2==0)
				{
					temp[k]=s[len-3-(j+k+1)];
				}
				else
				{
					temp[k]=s[len-3-(j+k-1)];
				}
			}

			try
			{
				n=getlong(temp);
				data.values[ch-1-i]=n;
				data.status[ch-1-i]=DataStatus::OK;
			}
			catch(std::exception& e)
			{
				data.status[ch-1-i]=DataStatus::InValid;
			}

		}

		return data;
	}

	PollData longInverseType(unsigned char *s,int len,int ch){
		std::string str1;
		int i=0,j=0;
		long n;
		PollData data;
		data.ReSize(ch);
		unsigned char temp[byteCountModebus/2];
		for (i = 0,j=0; i < ch; ++i,j=j+byteCountModebus/2) { //ch/2 - every channel is 2 bytes

			int k;
			for (k = 0; k < byteCountModebus/2; ++k) {
				temp[k]=(int)s[len-3-(j+byteCountModebus/2-1-k)];
			}
			try
			{
				n=getlong(temp);
				data.values[ch-1-i]=n;
				data.status[ch-1-i]=DataStatus::OK;

			}
			catch(std::exception& e)
			{
				data.status[ch-1-i]=DataStatus::InValid;
			}

		}

		return data;
	}

	PollData floatType(unsigned char *s,int len,int ch) {
		std::string str1;
		float f;
		int i=0,j=0;
		PollData data;
		data.ReSize(ch);
		unsigned char temp[byteCountModebus/2];


		for (i = 0,j=0; i < ch; ++i,j=j+byteCountModebus/2) { //ch/2 - every channel is 2 bytes
			int k;
			for (k = 0; k < byteCountModebus/2; ++k) {
				if(k%2==0)
				{
					temp[k]=s[len-3-(j+k+1)];
				}
				else
				{
					temp[k]=(int)s[len-3-(j+k-1)];
				}
			}


			try
			{
				f=getFloat(temp);
				data.values[ch-1-i]=f;
				data.status[ch-1-i]=DataStatus::OK;
			}
			catch(std::exception& e)
			{
				data.status[ch-1-i]=DataStatus::InValid;
			}

		}

		return data;
	}

	PollData floatInverseType(unsigned char *s,int len,int ch){
		std::string str1;
		float f;
		int i=0,j=0;
		PollData data;
		data.ReSize(ch);
		unsigned char temp[byteCountModebus/2];

		for (i = 0,j=0; i < ch; ++i,j=j+byteCountModebus/2) { //ch/2 - every channel is 2 bytes
			int k;
			for (k = 0; k < byteCountModebus/2; ++k) {
				temp[k]=s[len-3-(j+byteCountModebus/2-1-k)];
			}

			try
			{
				f=getFloat(temp);
				data.values[ch-1-i]=f;
				data.status[ch-1-i]=DataStatus::OK;
			}
			catch(std::exception& e)
			{
				data.status[ch-1-i]=DataStatus::InValid;
			}
		}

		return data;
	}

	PollData doubleType(unsigned char *s,int len,int ch){
		int byteCount=8;
		std::string str1;
		int i=0,j=0;
		PollData data;
		data.ReSize(ch);
		unsigned char temp[byteCount];

		for (i = 0,j=0; i < ch; ++i,j=j+byteCount) { //ch/2 - every channel is 2 bytes
			int k;
			for (k = 0; k < byteCount; ++k) {
				if(k%2==0)
				{
					temp[k]=s[len-3-(j+k+1)];
				}
				else
				{
					temp[k]=s[len-3-(j+k-1)];
				}
			}

			try
			{
				double d=getDouble(temp);
				data.values[ch-1-i]=d;
				data.status[ch-1-i]=DataStatus::OK;
			}
			catch(std::exception& e)
			{
				data.status[ch-1-i]=DataStatus::InValid;
			}

		}
		return data;
	}

	PollData doubleInverseType(unsigned char *s,int len,int ch){
		std::string str1;
		int i=0,j=0;
		unsigned char temp[byteCountModebus];
		PollData data;
		data.ReSize(ch);

		for (i = 0,j=0; i < ch; ++i,j=j+byteCountModebus) { //ch/2 - every channel is 2 bytes
			int k;
			for (k = 0; k < byteCountModebus; ++k) {
				temp[k]=s[len-3-(j+byteCountModebus-1-k)];
			}

			try
			{
				double d=getDouble(temp);
				data.values[ch-1-i]=d;
				data.status[ch-1-i]=DataStatus::OK;
			}
			catch(std::exception& e)
			{
				data.status[ch-1-i]=DataStatus::InValid;
			}

		}
		return data;
	}

};

#endif /* modbusc_Hpp */