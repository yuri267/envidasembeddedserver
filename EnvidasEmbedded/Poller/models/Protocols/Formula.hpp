#ifndef Formula_Hpp
#define Formula_Hpp

#include "ProtocolIncludes.hpp"

class Formula: public ProtocolFactory
{
public:

	Formula(device dev){this->channelsd=dev.getChannels().size();}

	~Formula(){}

	PollData start(){return poll();}

	string Name(){return "Formula";}

private:

	int channelsd;

	PollData poll()
	{
		PollData data;
		data.initZeroValue(channelsd);
		return data;
	}
};

#endif /* Formula_Hpp */