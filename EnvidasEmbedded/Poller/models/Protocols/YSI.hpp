#ifndef YSI_HPP_
#define YSI_HPP_

#include "ProtocolIncludes.hpp"

class YSI600 : public ProtocolFactory 
{
public:

	YSI600(device dev){
		this->channelsd=dev.getChannels().size();
		this->baundRate=dev.getBaundRate();
		this->port=dev.getPort();
		this->devNum=dev.getType();
		this->address = dev.getAddress();
	}
	~YSI600(){}

	PollData start(){return poll();}

   	string Name(){return "YSI600";}

private:
	int delay;
	int port;
	int devNum;
	int address;
	errorLog elog;
	int channelsd;
	int baundRate;

	PollData poll (){
		PollData data;
		string line = getData();
		data=parseData(line);
		return data;
	}

	string getData() {
		string line = "";
		ICommunication * sp = new serialPort(port, this->baundRate, 1, 1, 1, false);
		bool open = sp->OpenConnection();
		if (!open)
			elog.saveStringToBufferw("cannot open the port");
		else {

			sp->Send("\r");
			sp->Wait(1000000);

			sp->Send("data\r");
			sp->Wait(5000000);
			int i;
			unsigned char buf[sp->maxLineLentgh()];

			if ((i = sp->ReceiveRawByte(buf)) > 0) {
				line = line + charArrayTostring(buf, i);

			}
			sp->CloseConnection();

		}

		return line;
	}

	PollData parseData(string data){
		PollData pd;
		pd.ReSize(channelsd);
		if(data.find("data",0)<0)
		{
			pd.SetValueToAll(0);
			pd.SetStatusToall(4);
			return pd;
		}

		try
		{
			int first = data.find("data",0);
			int last = data.find("#",first);

			std::string linesub = data.substr(first+4,last-first-4);


			vector<string> splitD = split(linesub.c_str(), ' ');
			std::string *assd= new std::string[channelsd+2];
			int y=0;
			int k=0;

			for(k=0;(unsigned)k<splitD.size();k++)
			{
				if(splitD[k].length()!=0)
					assd[y++]=splitD[k];
			}

			int var = 0;
			for (var = 2; var < channelsd+2; ++var) {
					pd.values.at(var-2) = atof(assd[var].c_str());
					pd.status.at(var-2) = DataStatus::OK;
			}
			return pd;
		}
		catch(std::exception& e)
		{
			pd.SetValueToAll(-9999);
			pd.SetStatusToall(4);
		}
		return pd;

	}
};

#endif /* YSI_HPP_ */