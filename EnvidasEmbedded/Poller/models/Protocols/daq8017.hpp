#ifndef daq8017_Hpp
#define daq8017_Hpp

#include "ProtocolIncludes.hpp"


class daq8017 : public ProtocolFactory
{
public:
	
	daq8017(device dev){
		this->channelsd=dev.getChannels().size();
		this->baundRate=dev.getBaundRate();
		this->port=dev.getPort();
		this->devNum=dev.getType();
		this->address = dev.getAddress();
		this->calib = 0;
		this->status=DataStatus::OK;
		parseAdditionData(dev.getAdditionData());
	}

	~daq8017(){}
	
	PollData start(){return poll();}

   	string Name(){return "DAC 8017";}

private:

	int delay;
	int port;
	int devNum;
	int address;
	errorLog elog;
	int channelsd;
	int baundRate;

	bool calib;
	int status;


	void parseAdditionData(string data){
		int pos  = data.find("status=");
		if(pos>-1)
		{

			try
			{
				int ind = data.find(",",pos+7);
				string num = data.substr(pos+7,ind-pos-7);
				status = atoi(num.c_str());
			}
			catch(std::exception& e)
			{
				status = DataStatus::OK;
			}
		}
		else
		{
			status = DataStatus::OK;
		}

		pos  = data.find("calib=");

		if(pos>-1)
		{
			try
			{
				int ind = data.find(",",pos+6);
				string num = data.substr(pos+6,ind-pos-6);
				int bit = atoi(num.c_str());

				calib = (bit==1)?true:false;
			}
			catch(std::exception& e)
			{
				calib = false;
			}
		}
		else
		{
			calib = false;
		}
	}

	PollData poll (){
		PollData _data;
		if(calib)
		{
			_data.initZeroValue(channelsd);
			_data.SetStatusToall(status);
			sleep(1);
			return _data;
		}

			stringstream sendStream;
			if(address<10)
				sendStream << "#0" << address << "\r";
			else
				sendStream << "#" << address << "\r";
			string send = sendStream.str();
			//get a string line
			string line = getData(send);
			//value+status

			_data = parseData(line);


			return _data;
	}

	string getData(string send) {
		string line = "";
		ICommunication * sp = new serialPort(port, this->baundRate, 1, 1, 1, false);
		bool open = sp->OpenConnection();
		if (!open)
			elog.saveStringToBufferw("cannot open the port");
		else {

			sp->Send(send);
			sp->Wait(100000);

			int i;
			unsigned char buf[sp->maxLineLentgh()];

			if ((i = sp->ReceiveRawByte(buf)) > 0) {
				line = line + charArrayTostring(buf, i);

			}
			sp->CloseConnection();

		}

		return line;
	}
	
	PollData parseData(string data){
		int i = 0;
		int startpos = 1;
		string str;
		PollData _data; //data+status
		_data.ReSize(channelsd);
		int start = data.find(">");
		if(start < 0)
		{
			return _data;
		}

		try{
			char c = data.at(start);
			

			switch (c) {
			case '>': //ok
			{
				startpos = start+1;
				for (i = 0; i < channelsd; ++i) {
					str=data.substr(startpos,7);
					startpos+=7;
					try {
						_data.values[i] = (atof(str.c_str()));
						_data.status[i] = DataStatus::OK;
					} catch (std::exception& e) {
						_data.status[i] = DataStatus::CommFail;
					}
				}

				_data.SetStatusToall(status);

				break;
			}//error

			default:
				elog.saveStringToBufferw("no string from envidac");
				break;
			}
		}
		catch (std::exception& e) {
			elog.saveStringToBufferw("wrong Device address/id or unconnected Device");
			PollData d(channelsd);
			return d;
		}
		return _data;
	}

};
#endif /* daq8017_Hpp */