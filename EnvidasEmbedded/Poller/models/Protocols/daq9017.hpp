#ifndef daq9017_Hpp
#define daq9017_Hpp

#include "ProtocolIncludes.hpp"

class daq9017 : public ProtocolFactory
{
public:
	daq9017(device dev){
		this->channelsd=dev.getChannels().size();
		this->baundRate=dev.getBaundRate();
		this->port=dev.getPort();
		this->devNum=dev.getType();
		this->address = dev.getAddress();
		this->slot = dev.getAdditionData();
	}

	~daq9017(){}

	PollData start(){return poll();}

	string Name(){return "DAC 9017";}

private:
	int delay;
	int port;
	int devNum;
	int address;
	errorLog elog;
	int channelsd;
	int baundRate;
	string slot;

	PollData poll(){
		stringstream sendStream;
		if(address<10)
			sendStream << "#0" << address ;
		else
			sendStream << "#" << address ;
		int sl;
		try
		{
			sl=atoi(slot.c_str());
		}
		catch(std::exception &e )
		{
			sl=0;
		}
		sendStream<<"S"<<sl<< "\r";
		string send = sendStream.str();
			//get a string line
		string line = getData(send);
			//value+status
		PollData _data;
		_data = parseData(line);

		return _data;
	}

	string getData(string send) {
		string line = "";
		ICommunication * sp = new serialPort(port, this->baundRate, 1, 1, 1, false);
		bool open = sp->OpenConnection();
		if (!open)
			elog.saveStringToBufferw("cannot open the port");
		else {

			sp->Send(send);
			sp->Wait(100000);

			int i;
			unsigned char buf[sp->maxLineLentgh()];

			if ((i = sp->ReceiveRawByte(buf)) > 0) {
				line = line + charArrayTostring(buf, i);

			}
			sp->CloseConnection();

		}

		return line;
	}

	PollData parseData(string data) {

		int i = 0;
		int startpos = 1;
		string str;
		PollData _data; //data+status

		try{
			char c = data.at(0);
			_data.ReSize(channelsd);

			switch (c) {
					case '>': //ok
					{
						for (i = 0; i < channelsd; ++i) {
							str=data.substr(startpos,7);
							startpos+=7;
							try {
								_data.values[i] = (atof(str.c_str()));
								_data.status[i] = DataStatus::OK;
							} catch (std::exception& e) {
								_data.status[i] = DataStatus::CommFail;
							}
						}



						break;
					}//error

					default:
					elog.saveStringToBufferw("no string from envidac");
					break;
				}
			}
			catch (std::exception& e) {
				elog.saveStringToBufferw("wrong Device address/id or unconnected Device");
				PollData d;
				return d;
			}
			return _data;

		}

	};

#endif /* daq9017_Hpp */