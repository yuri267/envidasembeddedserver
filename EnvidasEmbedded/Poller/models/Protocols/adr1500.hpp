#ifndef ADR1500_HPP_
#define ADR1500_HPP_

#include "ProtocolIncludes.hpp"



class adr1500 : public ProtocolFactory
{
public:

	static bool IsAdrFirstTime;

	adr1500(device dev){
		this->channelsd=dev.getChannels().size();
		this->baundRate=dev.getBaundRate();
		this->port=dev.getPort();
		this->devNum=dev.getType();
		this->address = dev.getAddress();
		this->calib = false;
		this->status=DataStatus::OK;


		try
		{
			string tmp = dev.getAdditionData();

			int ind = tmp.find(",",0);

			if(ind>-1)
			{
				pollutantID = atoi(tmp.substr(0,ind).c_str());
				parseAdditionData(tmp);
			}
			else
			{
				pollutantID = atoi(tmp.c_str());
			}

		}
		catch(exception& e)
		{
			pollutantID = 0;
		}
	}

	~adr1500(){}

	PollData start(){return poll();}

	string Name(){return "ADR1500";}

private:
	int delay;
	int port;
	int devNum;
	int address;
	errorLog elog;
	int channelsd;
	int baundRate;
	int pollutantID;

	bool calib;
	int status;
	
	void parseAdditionData(string data) {
		int pos  = data.find("status=");

		if(pos>-1)
		{
			try
			{
				int ind = data.find(",",pos+7);
				string num = data.substr(pos+7,ind-pos-7);
				status = atoi(num.c_str());
			}
			catch(std::exception& e)
			{
				status = DataStatus::OK;
			}
		}
		else
		{
			status = DataStatus::OK;
		}

		pos  = data.find("calib=");

		if(pos>-1)
		{
			try
			{
				int ind = data.find(",",pos+6);
				string num = data.substr(pos+6,ind-pos-6);
				int bit = atoi(num.c_str());

				calib = (bit==1)?true:false;
			}
			catch(std::exception& e)
			{
				calib = false;
			}
		}
		else
		{
			calib = false;
		}
	}

	PollData poll (){
		PollData data;

		if(calib)
		{
			data.ReSize(channelsd);
			data.SetStatusToall(status);
			data.SetValueToAll(0);
			sleep(1);
			return data;
		}

		string line = getData();
		data=parseData(line);
		return data;
	}

	string getData() {
		string line = "";
		ICommunication * sp = new serialPort(port, this->baundRate, 1, 1, 1, false);
		bool open = sp->OpenConnection();
		if (!open)
			elog.saveStringToBufferw("cannot open the port");
		else
		{
			line = receiveString(sp,"out\r\n",1000000);
			int n = line.find("n/a",0);
			int out = line.find("OUTPUT",0);


			if((line.length()!=0 && n>=0) || IsAdrFirstTime)//check standby
			{

				elog.saveStringToBufferw(!IsAdrFirstTime ? ("ADR1500 got the res : " + line) : ("----Configure cycling----"));

				bool operate = SetToOperateMode(sp,5);

				if(operate)
				{
					switch(this->pollutantID)
					{
						case Pollutant::PM25:
						sp->Send("\r\n flowrate 1.52 \r\n");
						sp->WaitInSeconds(3);
						sp->Send("\r\n inlet 2 \r\n");
						sp->WaitInSeconds(3);

						//cout << "PM25 " <<endl;
						break;
						case Pollutant::PM10:
						sp->Send("\r\n flowrate 1.191 \r\n");
						sp->WaitInSeconds(3);
						sp->Send("\r\n inlet 1 \r\n");
						sp->WaitInSeconds(3);

						//cout << "PM10 " <<endl;
						break;
						case Pollutant::TSP:
						sp->Send("\r\n flowrate 2 \r\n");
						sp->WaitInSeconds(3);

						//cout << "TSP " <<endl;
						break;
					}


					sp->Send("Key enter \r\n");
					sp->WaitInSeconds(2);
					sp->Send("Key enter \r\n");
					sp->WaitInSeconds(2);





					line = receiveString(sp,"out\r\n",1000000);

					elog.saveStringToBufferw("ADR1500 After send params: " + line);

				}
				if(IsAdrFirstTime)
					IsAdrFirstTime=false;

			}
			else if (line.length()==0 || out<0)//make the command once again
			{
				//elog.saveStringToBufferw("ADR1500 out Try 1: " + line);
				sp->WaitInSeconds(3);
				line = receiveString(sp,"out\r\n",1000000);
				//elog.saveStringToBufferw("ADR1500 out Try 2: " + line);
				out = line.find("OUTPUT",0);
				if(line.length()==0 || out<0)
				{
					sp->WaitInSeconds(3);
					line = receiveString(sp,"out\r\n",1000000);
					//elog.saveStringToBufferw("ADR1500 out Try 3: " + line);
				}

			}
			sp->CloseConnection();

		}

		return line;
	}

	string receiveString (ICommunication * sp,string data,long delay){
		string line = "";
		sp->Send(data);
		sp->Wait(delay);
		int i;
		unsigned char buf[1000];
		if ((i = sp->ReceiveRawByte(buf)) > 0) {
			line = line + charArrayTostring(buf, i);
		}
		return line;
	}

	bool SetToOperateMode(ICommunication * sp,int maxtry) {
		int Try=0;
		string line="";

		while(Try<maxtry)
		{
			sp->Send("Key esc\r\n");
			sp->WaitInSeconds(2);

			line = receiveString(sp,"d\r\n",2000000);
			elog.saveStringToBufferw("ADR1500 Operate Trying " + line);

			int d = line.find("OPERATE",0);
			if(line.length()!=0 && d>=0)
				return true;
			else
				Try++;

		}
		return false;
	}

	PollData parseData(string line){
		PollData pd;
		pd.ReSize(channelsd);
		try
		{
			int first = line.find("PUT",0);

			if(!(first<0))
			{
				std::string linesub = line.substr(first+4,line.length()-first-4);

				std::vector<std::string> splitD = split(linesub.c_str(), ' ');
				std::string *assd= new std::string[channelsd];
				int y=0;

				for(int k=0;(unsigned)k<splitD.size();k++)
				{
					if(splitD[k].length()!=0 && splitD[k].compare(" ")!=0)
						assd[y++]=splitD[k];
				}

				for (int var = 0; var < channelsd; ++var)
				{
					pd.values.at(var) = atof(assd[var].c_str());
					pd.status.at(var) = DataStatus::OK;
				}
			}

		}
		catch(std::exception& e)
		{
			pd.SetValueToAll(DataStatus::InValid);
			pd.SetStatusToall(DataStatus::InValid);
		}

		pd.SetStatusToall(status);
		return pd;
	}
};

#endif /* ADR1500_HPP_ */