#ifndef ES642DUST_HPP_
#define ES642DUST_HPP_

#include "ProtocolIncludes.hpp"

class Es642Dust : public ProtocolFactory
{
public:
	
	Es642Dust(device dev){
		this->channelsd=dev.getChannels().size();
		this->baundRate=dev.getBaundRate();
		this->port=dev.getPort();
		this->devNum=dev.getType();
		this->address = dev.getAddress();
	}
	
	~Es642Dust(){}

	PollData start(){return poll();}

	string Name(){return "ES642Dust";}

private:

	int delay;
	int port;
	int devNum;
	int address;
	errorLog elog;
	int channelsd;
	int baundRate;

	PollData poll(){
		PollData data;
		int maxTry=2;
		int i=0;
		string line;
		while(i<maxTry)
		{
			line = getData();
			if ((signed)line.find("*", 0) > -1) {
				break;
			}
			i++;
		}
		if(i==maxTry)
		{
			data.ReSize(channelsd);
			elog.saveStringToBufferw("Wrong string from dust device, try 2 times to sample");
			return data;
		}


		return parseData(line);
	}

	string getData(){
		string line = "";
		ICommunication * sp = new serialPort(port, this->baundRate, 1, 1, 1, false);
		bool open = sp->OpenConnection();
		if (!open)
			elog.saveStringToBufferw("cannot open the port");
		else {
			sp->Wait(1000000);

			int i;
			unsigned char buf[sp->maxLineLentgh()];

			if ((i = sp->ReceiveRawByte(buf)) > 0) {
				line = line + charArrayTostring(buf, i);

			}
			sp->CloseConnection();

		}

		return line;
	}

	PollData parseData(string data) {
		PollData pd;
		pd.ReSize(channelsd);
		vector<string> splitD = split(data.c_str(), ',');

		int var = 0;
		for (var = 0; var < channelsd; ++var) {
			pd.values.at(var) = atof(splitD[var].c_str());
			pd.status.at(var) = DataStatus::OK;
		}

		int status = atoi(splitD[5].c_str());
				if (status > 3) {//above 3 is wrong
					pd.status.at(0) = DataStatus::InValid;
				}

				return pd;
	}
};

#endif /* ES642DUST_HPP_ */