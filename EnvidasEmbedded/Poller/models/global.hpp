#ifndef Global_HPP
#define Global_HPP

#include <iostream>
#include <sstream> 
#include <vector>
#include <string>
#include <unistd.h>
#include <stdio.h>
#include <ctime>
#include <stack>
#include <map>
#include <sys/time.h>
#include <math.h>

using namespace std;

const static string workingFolder ="/media/mmcblk0p1/poller/workingFolder";
//const static string workingFolder ="workingFolder";
const static string instantFileName ="instant";
const static string dataFileName ="data";
const static string dataCopyFileName ="dataCopy";

const static int MaxRecordsInFile = 10000;
const static string configurationFileName = "configuration.json";

const static string errorLogFile = "Error/error";



string charArrayTostringR(char* s, int n) {
	string ret = "";
	for (int i = 0; i < n; ++i) {
		ret = ret + s[i];
	}

	return ret;
}

string charArrayTostring(unsigned char* s, int n) {
	char c;
	string ret = "";
	for (int i = 0; i < n; ++i) {
		c = s[i];
		ret = ret + c;
	}

	return ret;
}

vector<string> split(const char *str, char c = ' ') {	
	vector<string> result;
	do {
		const char *begin = str;

		while (*str != c && *str)
			str++;

		result.push_back(string(begin, str));
	} while (0 != *str++);

	return result;
}

bool isnan_number(float f){
	return f!=f;	
}

double toRadians(double direction) {
	double radian = direction * (M_PIl / 180);
	return radian;
}

float toDegrees(double arc) {
	float degree = arc * (180 / M_PIl); 
	return degree;
}
#endif /* Global_HPP */