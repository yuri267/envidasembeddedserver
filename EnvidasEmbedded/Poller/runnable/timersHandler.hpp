#ifndef timersHandler
#define timersHandler

#include <cstdlib>
#include <pthread.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include "Timer.hpp"
#include "../models/DataContainer.hpp"
#include "../models/Protocols/ProtocolFactory.hpp"

using namespace std;

struct thread_data{
   int  thread_id;
   device dev;
   ProtocolFactory * protocol;
};

std::map<int,DataContainer> m;

// void *PollDataThread(void *threadarg){
// 	struct thread_data *my_data;
// 	my_data = (struct thread_data *) threadarg;
// 	errorLog er;
// 	int records = my_data->dev.getAvgTime()*60/my_data->dev.getsampleRate();

// 	cout << "Polling Thread ID : " << my_data->thread_id << endl;

// 	int id = my_data->thread_id;
// 	std::map<int,DataContainer>::iterator it = m.find(id);

// 	if(it!=m.end())
// 	{	
// 		//PollData polldata = my_data->protocol->start();
// 		PollData polldata(4);

// 		polldata.printPollData();
// 		it->second.AddObjectToStack(polldata);
		
// 		if(records<=it->second.StackSize())
// 		{
			
// 			stringstream oss;
//          	oss << "Save data for device" << it->second.StackSize() ;
//          	er.saveStringToBufferw(oss.str());
// 			it->second.AnalyzeAndSave();
// 		}
// 	}
// 	else
// 	{
// 		DataContainer dataContainer(my_data->dev);
// 		m.insert(std::make_pair(id,dataContainer));
// 	}

// 	stringstream oss;
//     oss << "Poll Data Thread ID : " << my_data->thread_id << " Protocol name: "<<my_data->protocol->Name();
//     er.saveStringToBufferw(oss.str());

// 	pthread_exit(NULL);
// }


// void *StartTimer(void *threadarg) {
//   struct thread_data *my_data;
//   pthread_t thread;
//    my_data = (struct thread_data *) threadarg;

//    cout << "Timer Thread ID : " << my_data->thread_id << endl;

//    int interval = my_data->dev.getsampleRate();

//    int id = my_data->thread_id;

//    std::map<int,DataContainer>::iterator it = m.find(id);


// 	if(it==m.end())
// 	{
// 		DataContainer dataContainer(my_data->dev);
// 		m.insert(std::make_pair(id,dataContainer));
// 	}
	

//    struct thread_data td;
//    td.thread_id = my_data->thread_id;
//    td.dev = my_data->dev;
//    td.protocol = my_data->protocol;

//    int rc;
//    while(1)
// 	{
// 		cout<<"after delay : " <<td.thread_id << endl;
// 		sleep(interval);
// 		rc = pthread_create(&thread, NULL, PollDataThread, (void *)&td);
// 		 if (rc){
// 		 	cout<<"Error:unable to create thread, id: " << td.thread_id<<" from "<< rc<<endl;
// 		 }
// 		 else
// 		 	pthread_join(thread, NULL);
// 	}

//    pthread_exit(NULL);
// }

void PollDataTask(thread_data data){
	int id = data.thread_id;
	std::map<int,DataContainer>::iterator it = m.find(id);

	if(it!=m.end())
	{	
		PollData polldata = data.protocol->start();
		it->second.AddObjectToStack(polldata);
	}
	else
	{
		DataContainer dataContainer(data.dev);
		m.insert(std::make_pair(id,dataContainer));
	}
	errorLog er;
	stringstream oss;
    oss << "Poll Data Thread ID : " << data.thread_id << " Protocol name: "<<data.protocol->Name();
    er.saveStringToBufferw(oss.str());
	// std::chrono::time_point<std::chrono::system_clock> time_point;
	// time_point = std::chrono::system_clock::now();

	// std::time_t ttp = std::chrono::system_clock::to_time_t(time_point);
	// std::cout << "time: " << std::ctime(&ttp);
}

void AverageDataTask(thread_data data){
	int id = data.thread_id;
	std::map<int,DataContainer>::iterator it = m.find(id);

	cout << "Average " <<data.thread_id << " Poll Data " <<endl;
	errorLog er;

	if(it!=m.end())
	{
		stringstream oss;
		oss << "Save data for device" << it->second.StackSize() ;
		er.saveStringToBufferw(oss.str());
		it->second.AnalyzeAndSave();
	}
	else
	{
		stringstream oss;
		oss << "Map Has no Thread Id : " << data.thread_id << " Protocol name: "<<data.protocol->Name();
		er.saveStringToBufferw(oss.str());
	}

	
	// std::chrono::time_point<std::chrono::system_clock> time_point;
	// time_point = std::chrono::system_clock::now();

	// std::time_t ttp = std::chrono::system_clock::to_time_t(time_point);
	// std::cout << "time: " << std::ctime(&ttp);
}

void StartPollDataTimer(thread_data data){
	device dev = data.dev;
	Timer timer;

	cout << "Timer Thread ID : " << data.thread_id << endl;
	int interval = dev.getsampleRate();
	cout << "Timer sample interval : " << interval << endl;

  	thread_data td;
   	td.thread_id = data.thread_id;
   	td.dev = dev;
   	td.protocol = data.protocol;

   	std::map<int,DataContainer>::iterator it = m.find(data.thread_id);

	if(it==m.end())
	{
		DataContainer dataContainer(dev);
		m.insert(std::make_pair(data.thread_id,dataContainer));
	}

    timer.start(chrono::seconds(interval), [&td]{
      PollDataTask(td);
    });
}

void StartAverageTimer(thread_data data){
	device dev = data.dev;
	Timer timer;

	cout << "Timer Thread ID : " << data.thread_id << endl;
	int interval = dev.getAvgTime()*60;
	cout << "Timer Average interval : " << interval << endl;

	thread_data td;
   	td.thread_id = data.thread_id;
   	td.dev = dev;
   	td.protocol = data.protocol;

	time_t now = time(0);
	int delaySec = interval - long(now)%interval;
	sleep(delaySec);

	auto f = std::async([td]{AverageDataTask(td);});
	f.get();

    timer.start(chrono::seconds(interval), [&td]{
      AverageDataTask(td);
    });
}

#endif /* timersHandler */