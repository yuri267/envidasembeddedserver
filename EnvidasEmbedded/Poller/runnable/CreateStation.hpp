#ifndef CreateStation
#define CreateStation

#include "../models/rapidjson/reader.h"
#include "../models/rapidjson/document.h"
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include "../models/station.hpp"

using namespace std;
using namespace rapidjson;


vector<Channel> getChannels(Value& channels){
	vector<Channel> channelParsed(channels.Size());
	for (int i = 0; i < channels.Size(); ++i)
    	{ 
    		auto& element = channels[i];
    		auto& celem = element["id"]; 
    		int id = celem.GetInt();
    		celem = element["linearA"];  
    		float linearA = (float)celem.GetDouble();
    		celem = element["linearB"];  
    		float linearB = (float)celem.GetDouble();
    		celem = element["minAvg"];  
    		float minAvg = (float)celem.GetDouble();
    		celem = element["averageType"];  
    		int averageType = celem.GetInt();
    		celem = element["Threashold"];  
    		int Threashold = celem.GetInt();
    		celem = element["additionchannels"];  
    		int additionchannels = celem.GetInt();
    		celem = element["units"];  
    		auto units = celem.GetString();
    		celem = element["Name"];  
    		auto Name = celem.GetString();
    		celem = element["additionData"];  
    		auto additionData = celem.GetString();
    		celem = element["formula"];  
    		auto formula = celem.GetString();
			celem = element["summary"];
			auto summary = celem.GetBool();
			Channel c(id,linearA,linearB,minAvg,averageType,Threashold,additionchannels,units,Name,additionData,formula,summary);
    		
    		channelParsed.at(i)=c;
    	}

    return channelParsed;
}

vector<device> getDevices(Value& devs){

	vector<device> devices(devs.Size());

	for (int i = 0; i < devs.Size(); ++i)
    	{ 
    		auto& element = devs[i];
    		vector<Channel> channels = getChannels(element["channels"]);
       		auto& celem = element["id"]; 
    		int id = celem.GetInt();
    		celem = element["port"]; 
    		int port = celem.GetInt();
    		celem = element["type"]; 
    		int type = celem.GetInt();
    		celem = element["Address"]; 
    		int Address = celem.GetInt();
    		celem = element["additionalData"]; 
    		string additionalData = celem.GetString();
    		celem = element["sampleRate"]; 
    		int sampleRate = celem.GetInt();
			celem = element["AvgTime"]; 
    		int AvgTime = celem.GetInt();
    		celem = element["baudRate"]; 
    		int baudRate = celem.GetInt();
    		device d(id,port,Address,baudRate,type,sampleRate,AvgTime,additionalData,channels);
			
			devices.at(i) = d;
		}
    return devices;
}

station stationRetrail(){	
	FILE *fileptr;

	long filelen;
	auto path= workingFolder+"/"+configurationFileName;
	fileptr = fopen(path.c_str(), "rb");  
	fseek(fileptr, 0, SEEK_END);         
	filelen = ftell(fileptr);             
	rewind(fileptr);                     

	auto str = (char *)malloc((filelen+1)*sizeof(char)); 
	fread(str, filelen, 1, fileptr); 
	fclose(fileptr); 
	

	Document document;
	document.Parse<0>(str).HasParseError();
	document.Parse(str);
	if(document.IsObject())        // add this line
	{
		vector<device> devices = getDevices(document["devices"]);
		auto& celem = document["name"];
		auto name = celem.GetString();
		celem = document["id"];
		int id = celem.GetInt();
		celem = document["latitude"];
		auto latitude = celem.GetString();
		celem = document["longitude"];
		auto longitude = celem.GetString();

		station sta = station(name,id,latitude,longitude,devices);
		return sta;
	}


	// vector<Channel> Channels;

	// vector<Channel> ChannelsSecondDevice;

	// Channel c1(0,2,0.5,0,1,75,-1,"mm","c1","","");
	// Channel c2(1,2,0.5,0,8,75,-1,"mm","c2","","");
	// Channel c3(2,2,0.5,0,5,75,-1,"mm","c3","","");
	// Channel c4(3,2,0.5,0,7,75,-1,"mm","c4","","");


	// Channel c5(0,2,0.5,0,1,75,-1,"mm","c1","","");
	// Channel c6(1,2,0.5,0,8,75,-1,"mm","c2","","");
	// Channel c7(2,2,0.5,0,5,75,-1,"mm","c3","","");
	// Channel c8(3,2,0.5,0,7,75,-1,"mm","c4","","");
	
	// Channels.push_back(c1);
	// Channels.push_back(c2);
	// Channels.push_back(c3);
	// Channels.push_back(c4);

	// ChannelsSecondDevice.push_back(c5);
	// ChannelsSecondDevice.push_back(c6);
	// ChannelsSecondDevice.push_back(c7);
	// ChannelsSecondDevice.push_back(c8);


	// vector<device> devs(2);
	
	// devs.at(0) = device(0,2,1,9600,0,20,3,"asd",Channels);
	// devs.at(1) = device(1,0,0,9600,3,30,5,"3,0,49,0,4,1,",ChannelsSecondDevice);
	// station sta = station("test",1,"12","21",devs);
	//return sta;
}



#endif /* CreateStation */