#ifndef timer_HPP_
#define timer_HPP_

#include <iostream>
#include <thread>
#include <chrono>
#include <future>

using namespace std;

class Timer
{
private:

    thread th;
    bool running;
    
public:
    typedef std::chrono::milliseconds Interval;
    typedef std::function<void(void)> Timeout;

    Timer(){running= false;}
    void start(const Interval &interval,
               const Timeout &timeout)
    {
        running = true;

        th = thread([=]()
        {
            while (running == true) {
                this_thread::sleep_for(interval);
                timeout();
            }
        });

        th.join();
    }

    void stop()
    {
        running = false;
    }
};

#endif /* timer_HPP_ */