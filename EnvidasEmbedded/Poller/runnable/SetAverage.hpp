#ifndef SetAverage_HPP_
#define SetAverage_HPP_

#include "../models/Average/AverageFactory.hpp"
#include "../models/Average/Mean.hpp"
#include "../models/Average/Sum.hpp"
#include "../models/Average/Maximum.hpp"
#include "../models/Average/Minimum.hpp"
#include "../models/Average/YamartinoStandartDeviation.hpp"
#include "../models/Average/WindSpeed.hpp"
#include "../models/Average/WindDirection.hpp"
#include "../models/Average/SigmaTeta.hpp"
#include "../models/Average/StandardDeviation.hpp"

 IAverage * make_average(int type,bool summary)
 {
 	switch(type)
 	{
 		case mean : return new Mean(summary);
 		case sum : return new Sum(summary);
 		case maximum : return new Maximum();
 		case minimum : return new Minimum();
 		case yamartinoStandartDeviation : return new YamartinoStandartDeviation();
 		case windspeed: return new WindSpeed();
 		case unit_Vector_Direction : 
 		case winddirection : return new WindDirection();
 		case sigma_Teta : return new SigmaTeta();
 		case standardCondition : return new StandardDeviation();
 		default : return new Mean();
 	}
 }


#endif /* SetAverage_HPP_ */