#ifndef SetProtocol_HPP_
#define SetProtocol_HPP_

#include "../models/Protocols/Es642Dust.hpp"
#include "../models/Protocols/daq8017.hpp"
#include "../models/Protocols/Formula.hpp"
#include "../models/Protocols/daq8080.hpp"
#include "../models/Protocols/modbus.hpp"
#include "../models/Protocols/YSI.hpp"
#include "../models/Protocols/zdcs4420.hpp"
#include "../models/Protocols/daq9017.hpp"
#include "../models/Protocols/twoBOzone.hpp"
#include "../models/Protocols/rmYoung32500.hpp"
#include "../models/Protocols/adr1500.hpp"

ProtocolFactory * ProtocolFactory::make_protocol(device d)
{
	int type = d.getType();
	deviceEnum val = static_cast<deviceEnum>(type);

	switch(val)
	{
		case  d8017 : return new daq8017(d);	
		case  d8080 : return new daq8080(d);
		case  modbusc : return new modbus(d);
		case  dYSI600OMS : return new YSI600(d);
		case  ZDCS4420 : return new zdcs4420(d);
		case  d9017 : 	return new daq9017(d);
		case  TwoBOzone : return new twoBOzone(d);
		case  ES642Dust : return new Es642Dust(d);
		case  rmY32500 : return new  rmYoung32500(d);
		case  ADR1500 : return new adr1500(d);
		case  virtualFormula :
		default     : return new Formula(d);
	}
}

#endif /* SetProtocol_HPP_ */