const exec = require('child_process').exec;

var options = {
  		encoding: 'utf8'
	};

module.exports = {
		setTimeCommand : function(req,res){	
		var date = req.body.date;
		exec("date -s '" + date+"'", function(code, stdout, stderr) {		
			res.setHeader('Content-Type','application/json');
			res.send(JSON.stringify({
			date : stdout}));
	});
}

};
