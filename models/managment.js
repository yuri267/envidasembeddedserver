var fs = require('fs');
var settings = require('../global/settings');

module.exports = {

	getErrorLogFile : function(req,res){
		var filename = settings.PollerWorkingPath+settings.ErrorFolder+"error"+req.params.day+".txt";
		var result = [];
		fs.readFile(filename,function(err,content){
			if(err){
				res.status(501);
				res.send(JSON.stringify({data : err}));
			}
			else{
				var recordsArr = content.toString().split('\r\n').
    							map(function (val) {
    								if(val != undefined && val.length > 10 )
    									return JSON.parse(val);
    							});

				
				for (var i = 0; i <recordsArr.length; i++) {
					if(recordsArr[i] != undefined)
					{
						result.push({data : recordsArr[i] });	
						
					}
				}
				res.setHeader('Content-Type','application/json');
				res.send(JSON.stringify({log : result})); 
			}
		});
		
	}

}