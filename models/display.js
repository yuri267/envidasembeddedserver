var fs = require('fs');
var settings = require('../global/settings');

module.exports = {

	DevicesDisplay : function(req,res){
		var result = [];
		var filename = settings.PollerWorkingPath+settings.configurationFile;

		fs.readFile(filename,function(err,content){
			if(err){
				res.status(501);
				res.send(JSON.stringify({data : err}));
			}
			else{
				var parseJson = JSON.parse(content);
				var keys = parseJson['devices'];
				(function next(index) {
    			if (index === keys.length) { // No items left
    				res.setHeader('Content-Type','application/json');
    				res.send(JSON.stringify(result));
    				return;
    			}
    			var key = keys[index];
    			var devId = key.id
    			var devFilename = settings.PollerWorkingPath+"instant-"+devId+".txt";

    			fs.readFile(devFilename, function(err, contentDev){
    				if (!err)
    				{
    					var parseJsonInstant = JSON.parse(contentDev);

    					for (var i = 0; i < parseJsonInstant.channels.length; i++) {
    						parseJsonInstant.channels[i].instantValue = parseJsonInstant.channels[i].value;
    						parseJsonInstant.channels[i].instantStatus = parseJsonInstant.channels[i].status;
    						delete parseJsonInstant.channels[i].value;
    						delete parseJsonInstant.channels[i].status;
    					}

    					var dataFileName = settings.PollerWorkingPath+"data-"+devId+".txt";	

    					fs.readFile(dataFileName,function(err,datacontent){
    						if(err){
    							res.status(501);
    							res.send(JSON.stringify({message : err}));
    						}
    						else{

    							var recordsArr = datacontent.toString().split('\r\n').
    							map(function (val) {
    								if(val != undefined && val.length > 10 )
    									return JSON.parse(val);
    							});


    							var array = [];
    							for (var k = recordsArr.length-1; k >=0; k--) {
    								if(recordsArr[k] != undefined)
    								{
    									array.push(recordsArr[k]);
    									break;
    								}
    							}

    							var latestData = array[0];

    							for (var i = 0; i < latestData.channels.length; i++) {
    								
    								parseJsonInstant.channels[i].AverageValue = latestData.channels[i].value
    								parseJsonInstant.channels[i].AverageStatus = latestData.channels[i].status
    								
    								if(latestData.channels[i].Summary!=undefined)
    								{
    									var Summary = latestData.channels[i].Summary;
    									parseJsonInstant.channels[i].Summary=Summary;	
    								}
    								else
    								{
    									parseJsonInstant.channels[i].Summary={};
    								}
    							}
    							
    							result.push({deviceId : devId ,data : parseJsonInstant });	
    							next(index + 1);
    						}
    					});



    				} 
    			});   


    		})(0);
    	}
    	});
	},

	DeviceDisplay : function(req,res){
		var filename = settings.PollerWorkingPath+"instant-"+req.params.id+".txt";	
		var result = [];
		fs.readFile(filename,function(err,content){
			if(err){
				res.status(501);
				res.send(JSON.stringify({message : err}));
			}
			else{
				var parseJson = JSON.parse(content);

				for (var i = 0; i < parseJson.channels.length; i++) {
					parseJson.channels[i].instantValue = parseJson.channels[i].value;
					parseJson.channels[i].instantStatus = parseJson.channels[i].status;
					delete parseJson.channels[i].value;
					delete parseJson.channels[i].status;
				}
				
				var dataFileName = settings.PollerWorkingPath+"data-"+req.params.id+".txt";	

				fs.readFile(dataFileName,function(err,datacontent){
					if(err){
						res.status(501);
						res.send(JSON.stringify({message : err}));
					}
					else{
						var recordsArr = datacontent.toString().split('\r\n').
						map(function (val) {
							if(val != undefined && val.length > 10 )
								return JSON.parse(val);
						});
						

						var array = [];
						for (var i = recordsArr.length-1; i >=0; i--) {
							if(recordsArr[i] != undefined)
							{
								array.push(recordsArr[i]);
								break;
							}
						}
						var latestData = array[0];

						for (var i = 0; i < latestData.channels.length; i++) {
							
							parseJson.channels[i].AverageValue = latestData.channels[i].value
							parseJson.channels[i].AverageStatus = latestData.channels[i].status
							
							if(latestData.channels[i].Summary!=undefined)
							{
								var Summary = latestData.channels[i].Summary;
								parseJson.channels[i].Summary=Summary;	
							}
							else
							{
								parseJson.channels[i].Summary={};
							}
						}

						result.push({deviceId : req.params.id ,data : parseJson });	
						res.setHeader('Content-Type','application/json');
						res.send(JSON.stringify(result));
					}
				})


				

			}
		})
	},

	ChannelDisplay : function(req,res){
		var filename = settings.PollerWorkingPath+"instant-"+req.params.id+".txt";	
		var channelId = req.params.channel
		var result = [];
		fs.readFile(filename,function(err,content){
			if(err){
				res.status(501);
				res.send(JSON.stringify({message : err}));
			}
			else{
				var parseJson = JSON.parse(content);

				var channelArr = []

				var channelInstant = parseJson.channels[channelId]

				channelInstant.instantValue = parseJson.channels[channelId].value;
				channelInstant.instantStatus = parseJson.channels[channelId].status;
				delete channelInstant.value;
				delete channelInstant.status;
				
				

				var dataFileName = settings.PollerWorkingPath+"data-"+req.params.id+".txt";	

				fs.readFile(dataFileName,function(err,datacontent){
					if(err){
						res.status(501);
						res.send(JSON.stringify({message : err}));
					}
					else{

						var recordsArr = datacontent.toString().split('\r\n').
						map(function (val) {
							if(val != undefined && val.length > 10 )
								return JSON.parse(val);
						});
						

						var array = [];
						for (var i = recordsArr.length-1; i >=0; i--) {
							if(recordsArr[i] != undefined)
							{
								array.push(recordsArr[i]);
								break;
							}
						}
						var latestData = array[0];

						channelInstant.AverageValue = latestData.channels[channelId].value
						channelInstant.AverageStatus = latestData.channels[channelId].status

						if(latestData.channels[channelId].Summary!=undefined)
						{
							var Summary = latestData.channels[channelId].Summary;
							channelInstant.Summary=Summary;	
						}
						else
						{
							channelInstant.Summary={};
						}

						channelArr.push(channelInstant);

						parseJson.channels = channelArr;

						result.push({deviceId : req.params.id ,data : parseJson });	
						res.setHeader('Content-Type','application/json');
						res.send(JSON.stringify(result));
					}
				});


				

			}
		});
	}
}