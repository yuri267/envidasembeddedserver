var fs = require('fs');
var settings = require('../global/settings');

module.exports = {
	
	getLatest : function(req,res){
		var result = [];

		var filename = settings.PollerWorkingPath+settings.configurationFile;
		fs.readFile(filename,function(err,content){
			if(err){
				res.status(501);
				res.send(JSON.stringify({data : err}));
			}
			else{
				var parseJson = JSON.parse(content);
				
				var keys = parseJson['devices'];

				(function next(index) {
    			if (index === keys.length) { // No items left
    				res.setHeader('Content-Type','application/json');
    				res.send(JSON.stringify(result));
    				return;
    			}
    			var key = keys[index];
    			var devId = key.id
    			var devFilename = settings.PollerWorkingPath+"instant-"+devId+".txt";

    			fs.readFile(devFilename, function(err, contentDev){
    				if (!err)
    				{
    					var parseJsonDev = JSON.parse(contentDev);
    					result.push({deviceId : devId ,data : parseJsonDev });	
    					next(index + 1);
    				} 
    			});   


    		})(0);
    	}
    })
	},

	getDeviceLatest : function(req,res){
		var filename = settings.PollerWorkingPath+"instant-"+req.params.id+".txt";
		fs.readFile(filename,function(err,content){
			if(err){
				res.status(501);
				res.send(JSON.stringify({message : err}));
			}
			else{
				var result = [];
				var parseJson = JSON.parse(content);
				result.push({deviceId : req.params.id ,data : parseJson });	
				res.setHeader('Content-Type','application/json');
				res.send(JSON.stringify(result));
			}
		})
	},

	getData : function(req,res){
		var filename = settings.PollerWorkingPath+settings.configurationFile;
		var from = Date.parse(req.query.from)/1000;
		var to = Date.parse(req.query.to)/1000;
		var result = [];

		fs.readFile(filename,function(err,content){
			if(err){
				res.status(501);
				res.send(JSON.stringify({data : err}));
			}
			else{
				var parseJson = JSON.parse(content);
				var keys = parseJson['devices'];


				(function next(index) {
    			if (index === keys.length) { // No items left
    				res.setHeader('Content-Type','application/json');
    				res.send(JSON.stringify(result));
    				return;
    			}
    			var key = keys[index];
    			var devId = key.id
    			var devFilename = settings.PollerWorkingPath+"data-"+devId+".txt";

    			fs.readFile(devFilename, function(err, contentDev){
    				if (!err)
    				{
    					var recordsArr = contentDev.toString().split('\r\n').
    					map(function (val) {
    						if(val != undefined && val.length > 10 )
    							return JSON.parse(val);
    					});

    					var array = [];

    					for (var i = 0; i < recordsArr.length; i++) {
    						if(recordsArr[i] != undefined)
    							array.push(recordsArr[i]);
    					}

    					var arrFound = array.filter(function(item) {
    						return item.time >= from && item.time <= to;
    					});

    					result.push({deviceId : devId ,data : arrFound});	
    					next(index + 1);
    				}
    				else
    				{
    					result.push({deviceId : devId ,data :[] });	
    					next(index + 1);
    				} 
    			});   


    		})(0);

    		}
    	});
	},

	getDeviceData : function(req,res){
		var filename = settings.PollerWorkingPath+"data-"+req.params.id+".txt";	
		var from = Date.parse(req.query.from)/1000;
		var to = Date.parse(req.query.to)/1000;
		var result = [];

		fs.readFile(filename,function(err,content){
			if(err){
				res.status(501);
				res.send(JSON.stringify({message : err}));
			}
			else{
				var recordsArr = content.toString().split('\r\n').
				map(function (val) {
					if(val != undefined && val.length > 10 )
						return JSON.parse(val);
				});

				var array = [];

				for (var i = 0; i < recordsArr.length; i++) {
					if(recordsArr[i] != undefined)
						array.push(recordsArr[i]);
				}

				var arrFound = array.filter(function(item) {
					return item.time >= from && item.time <= to;
				});

				result.push({deviceId : req.params.id ,data : arrFound });
				res.setHeader('Content-Type','application/json');
				res.send(JSON.stringify(result));
			}
		})
	},

	getDataFileSize : function(req,res){
		var result = [];

		var filename = settings.PollerWorkingPath+settings.configurationFile;
		fs.readFile(filename,function(err,content){
			if(err){
				res.status(501);
				res.send(JSON.stringify({message : err}));
			}
			else
			{

				var parseJson = JSON.parse(content);
				var keys = parseJson['devices'];

				for (var i = 0; i < keys.length; i++) {
					var devId = keys[i].id
					var devFilename = settings.PollerWorkingPath+"data-"+devId+".txt";
					var stats = fs.statSync(devFilename)
					var fileSizeInBytes = stats.size
					result.push({deviceId : devId ,fileSize : fileSizeInBytes });
				}

				res.setHeader('Content-Type','application/json');
				res.send(JSON.stringify(result));
			}
		})
	},

	getDeviceDataFileSize : function(req,res){
		var filename = settings.PollerWorkingPath+"data-"+req.params.id+".txt";
		const stats = fs.statSync(filename)
		const fileSizeInBytes = stats.size
		res.setHeader('Content-Type','application/json');
		
		var result = [];
		result.push({deviceId : req.params.id ,fileSize : fileSizeInBytes });	
		res.send(JSON.stringify(result)); 
	},

	getChannelData : function(req,res){
		var filename = settings.PollerWorkingPath+"data-"+req.params.id+".txt";	
		var from = Date.parse(req.query.from)/1000;
		var to = Date.parse(req.query.to)/1000;
		var result = [];
		var channel = req.params.channel

		fs.readFile(filename,function(err,content){
			if(err){
				res.status(501);
				res.send(JSON.stringify({message : err}));
			}
			else{
				var recordsArr = content.toString().split('\r\n').
				map(function (val) {
					if(val != undefined && val.length > 10 )
						return JSON.parse(val);
				});

				var array = [];

				for (var i = 0; i < recordsArr.length; i++) {
					if(recordsArr[i] != undefined)
						array.push(recordsArr[i]);
				}

				var arrFound = array.filter(function(item) {
					return item.time >= from && item.time <= to;
				});

				var obj = []

				for (var i = 0; i < arrFound.length; i++) {
					var channelData = arrFound[i].channels.filter(function(item){
						return item.id==channel;
					});

					obj.push({time:arrFound[i].time,channels:channelData});
				}

				result.push({deviceId : req.params.id ,data : obj });
				res.setHeader('Content-Type','application/json');
				res.send(JSON.stringify(result));

			}
		});
	},

};


