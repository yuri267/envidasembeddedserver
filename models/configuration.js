var fs = require('fs');
var settings = require('../global/settings');

var filename = settings.PollerWorkingPath+settings.configurationFile;
	
module.exports = {
		saveConfiguration : function(req,res){
		fs.writeFile(filename,JSON.stringify(req.body),function(err){
			res.setHeader('Content-Type','application/json');
				if(err){
					res.status(501);
					res.send(JSON.stringify({configuration : err}));
				}
				else{
					res.send(JSON.stringify({configuration : 'success'}));
				}
			})
		},

		readConfiguration : function(req,res){
			res.setHeader('Content-Type','application/json');
			fs.readFile(filename,function(err,content){
				if(err){
					res.status(501);
					res.send(JSON.stringify({configuration : err}));
				}
				else{
					var parseJson = JSON.parse(content);
					res.send(JSON.stringify(parseJson));
				}
			})
			
		}

};