var mongoose = require('mongoose')
, Schema = mongoose.Schema;

var userSchema = new Schema({
  user : {type: String, unique : true, required	: true},
  pass : {type: String, unique : true, required : true},
  isAdmin: {type: Boolean, default: false}
});

module.exports  = mongoose.model('user', userSchema);
