exports.HttpsWebPort = 8000;
exports.ListenIp = '192.168.0.80';

exports.MongoDBConnection = 'mongodb://localhost:22222/EnvidasServer'

exports.PollerWorkingPath = 'EnvidasEmbedded/Poller/workingFolder/'

exports.configurationFile = 'configuration.json'

exports.ErrorFolder = 'Error/'

